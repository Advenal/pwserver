#!/bin/sh

echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ":::                Perfect World Server 1.4.2v27               :::"
echo "                     http://kn1fe-zone.ru                         "
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ":::                    Stopping The Server                     :::"
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
pkill glinkd
pkill -x gs
pkill logservice
pkill uniquenamed
pkill gamedbd
pkill gdeliveryd
pkill gfactiond
pkill gacd
pkill -f org\.mono\.Authd
sleep 1
pkill -9 glinkd
pkill -9 -x gs
pkill -9 logservice
pkill -9 uniquenamed
pkill -9 gamedbd
pkill -9 gdeliveryd
pkill -9 gfactiond
pkill -9 gacd
pkill -9 -f org\.mono\.Authd
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ":::                        Server Offline                      :::"
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
