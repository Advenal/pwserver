#!/bin/sh
cd /home/mauthd/build

export HOME=/usr/lib/jvm/java-1.5.0-sun-1.5.0.22
export PATH=$HOME/bin:$PATH

export CLASSPATH=.:/usr/lib/jvm/java-1.5.0-sun-1.5.0.22/lib/dt.jar:/usr/lib/jvm/java-1.5.0-sun-1.5.0.22/lib/tools.jar:/usr/lib/jvm/java-1.5.0-sun-1.5.0.22/lib/mysql-connector-java-3.0.16-ga-bin.jar

export JAVA_HOME=/usr/lib/jvm/java-1.5.0-sun-1.5.0.22/

java -cp ../lib/mysql-connector-java-5.1.7-bin.jar:.:../lib/jio.jar:../lib/application.jar:../lib/commons-collections-3.1.jar:../lib/commons-dbcp-1.2.1.jar:../lib/commons-pool-1.2.jar:../lib/commons-logging-1.0.4.jar:../lib/log4j-1.2.9.jar:.:$CLASSPATH org.mono.Authd

