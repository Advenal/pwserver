#!/bin/sh
PW_PATH=/home

if [ ! -d $PW_PATH/logs ]; then
mkdir $PW_PATH/logs
fi

echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ":::                Perfect World Server 1.4.2v27               :::"
echo "                     http://kn1fe-zone.ru                         "
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ":::                        Start Server                        :::"
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
sleep 1
echo ""
echo "::: iWeb :::"
/usr/local/jakarta/bin/startup.sh
sleep 1
echo "::: Ready :::"
echo ""
echo "::: Log Service :::"
cd $PW_PATH/logservice; ./logservice logservice.conf >$PW_PATH/logs/logservice.log &
sleep 1
echo "::: Ready :::"
echo ""
echo "::: Unique Name Daemon :::"
cd $PW_PATH/uniquenamed; ./uniquenamed gamesys.conf >$PW_PATH/logs/uniquenamed.log &
sleep 1
echo "::: Ready :::"
echo ""
echo "::: Auth Daemon :::"
cd $PW_PATH/authd/build/; ./authd.sh &
sleep 2
echo "::: Ready :::"
echo ""
echo "::: Game Data Base Daemon :::"
cd $PW_PATH/gamedbd; ./gamedbd gamesys.conf >$PW_PATH/logs/gamedbd.log &
sleep 1
echo "::: Ready :::"
echo ""
echo "::: Game Anti Cheat Daemon :::"
cd $PW_PATH/gacd; ./gacd gamesys.conf >$PW_PATH/logs/gacd.log &
sleep 1
echo "::: Ready :::"
echo ""
echo "::: Game Faction Daemon :::"
cd $PW_PATH/gfactiond; ./gfactiond gamesys.conf >$PW_PATH/logs/gfactiond.log &
sleep 1
echo "::: Ready :::"
echo ""
echo "::: Game Delivery Daemon :::"
cd $PW_PATH/gdeliveryd; ./gdeliveryd gamesys.conf >$PW_PATH/logs/gdeliveryd.log &
sleep 1
echo "::: Ready :::"
echo ""
echo "::: Game Link Daemon :::"
cd $PW_PATH/glinkd; ./glinkd gamesys.conf 1 >$PW_PATH/logs/glink.log &
#cd $PW_PATH/glinkd; ./glinkd gamesys.conf 2 >$PW_PATH/logs/glink2.log &
#cd $PW_PATH/glinkd; ./glinkd gamesys.conf 3 >$PW_PATH/logs/glink3.log &
#cd $PW_PATH/glinkd; ./glinkd gamesys.conf 4 >$PW_PATH/logs/glink4.log &
sleep 7
echo "::: Ready :::"
echo ""
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ":::                            Maps                            :::"
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ""
echo "::: Game Service :::"
cd $PW_PATH/gamed; ./gs gs01 >$PW_PATH/logs/gs01.log &
sleep 10
echo "::: Ready :::"
echo ""
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ":::                        Server Online                       :::"
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo ""