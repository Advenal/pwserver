/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.19
 * Generated at: 2012-12-23 23:29:10 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.role;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.lang.*;
import protocol.*;
import com.goldhuman.auth.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.goldhuman.util.LocaleUtil;
import com.goldhuman.util.*;

public final class saverole_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(2);
    _jspx_dependants.put("/role/../include/header.jsp", Long.valueOf(1341308148000L));
    _jspx_dependants.put("/role/../include/foot.jsp", Long.valueOf(1341308148000L));
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

int roleid = Integer.parseInt(request.getParameter("roleid"));

RoleBean role = (RoleBean)session.getAttribute("gamedb_rolebean");
role.base.id = roleid;

//role.user.cash = Integer.parseInt(request.getParameter("cash"));

role.status.level = Integer.parseInt(request.getParameter("level"));
role.status.level2 = Integer.parseInt(request.getParameter("level2"));
role.status.exp = Integer.parseInt(request.getParameter("exp"));
role.status.sp = Integer.parseInt(request.getParameter("sp"));
role.status.pp = Integer.parseInt(request.getParameter("pp"));
role.status.posx = Integer.parseInt(request.getParameter("posx"));
role.status.posy = Integer.parseInt(request.getParameter("posy"));
role.status.posz = Integer.parseInt(request.getParameter("posz"));
role.status.reputation = Integer.parseInt(request.getParameter("reputation"));
if( (new String("on")).equals(request.getParameter("clearstorehousepasswd")) )
	role.status.storehousepasswd.clear();

role.pocket.money = Integer.parseInt(request.getParameter("money"));

role.ep.vitality = Integer.parseInt(request.getParameter("vitality"));
role.ep.energy = Integer.parseInt(request.getParameter("energy"));
role.ep.strength = Integer.parseInt(request.getParameter("strength"));
role.ep.agility = Integer.parseInt(request.getParameter("agility"));

role.ep.max_hp = Integer.parseInt(request.getParameter("max_hp"));
role.ep.max_mp = Integer.parseInt(request.getParameter("max_mp"));
role.ep.attack = Integer.parseInt(request.getParameter("attack"));
role.ep.damage_low = Integer.parseInt(request.getParameter("damage_low"));
role.ep.damage_high = Integer.parseInt(request.getParameter("damage_high"));

role.ep.addon_damage_low[0] = Integer.parseInt(request.getParameter("addon_damage_low0"));
role.ep.addon_damage_high[0] = Integer.parseInt(request.getParameter("addon_damage_high0"));
role.ep.addon_damage_low[1] = Integer.parseInt(request.getParameter("addon_damage_low1"));
role.ep.addon_damage_high[1] = Integer.parseInt(request.getParameter("addon_damage_high1"));
role.ep.addon_damage_low[2] = Integer.parseInt(request.getParameter("addon_damage_low2"));
role.ep.addon_damage_high[2] = Integer.parseInt(request.getParameter("addon_damage_high2"));
role.ep.addon_damage_low[3] = Integer.parseInt(request.getParameter("addon_damage_low3"));
role.ep.addon_damage_high[3] = Integer.parseInt(request.getParameter("addon_damage_high3"));
role.ep.addon_damage_low[4] = Integer.parseInt(request.getParameter("addon_damage_low4"));
role.ep.addon_damage_high[4] = Integer.parseInt(request.getParameter("addon_damage_high4"));

role.ep.damage_magic_low = Integer.parseInt(request.getParameter("damage_magic_low"));
role.ep.damage_magic_high = Integer.parseInt(request.getParameter("damage_magic_high"));

role.ep.resistance[0] = Integer.parseInt(request.getParameter("resistance0"));
role.ep.resistance[1] = Integer.parseInt(request.getParameter("resistance1"));
role.ep.resistance[2] = Integer.parseInt(request.getParameter("resistance2"));
role.ep.resistance[3] = Integer.parseInt(request.getParameter("resistance3"));
role.ep.resistance[4] = Integer.parseInt(request.getParameter("resistance4"));

role.ep.defense = Integer.parseInt(request.getParameter("defense"));
role.ep.armor = Integer.parseInt(request.getParameter("armor"));
role.ep.max_ap = Integer.parseInt(request.getParameter("max_ap"));

boolean success = false;
try {
	success = GameDB.update( role );
	if( success && role.base.id >= 16 && role.base.id < 31 )
	{   
		int newroleid = ( 0 == role.base.id % 2 ) ? (role.base.id+1) : (role.base.id-1);
		role.base.id = newroleid;
		success = GameDB.update( role );
	}
}
catch (Exception e) { out.println(e.toString()); return; }
LogFactory.getLog("saverole.jsp").info("putRoleInfo, "+role.getLogString()+",result="+success+",operator=" + AuthFilter.getRemoteUser(session) );

      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<link href=\"../include/style.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("<title>");
      out.print( LocaleUtil.getMessage(request,"role_saverole_title") );
      out.write("</title>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("\n");
      out.write("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n");
      out.write("\t<tr>\n");
      out.write("\t\t<td height=\"8\" colspan=\"3\" class=\"topline\"></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t\t<td width=\"225\" height=\"54\" bgcolor=\"#9E0B0E\">\n");
      out.write("\t\t\t&nbsp;\n");
      out.write("\t\t</td>\n");
      out.write("\t\t<td width=\"40\" bgcolor=\"#9E0B0E\">\n");
      out.write("\t\t\t&nbsp;\n");
      out.write("\t\t</td>\n");
      out.write("\t\t<td width=\"500\" class=\"navColor\">\n");
      out.write("\t\t\t(w2i) \n");
      out.write("\t\t\t");
      out.print( LocaleUtil.getMessage(request,"include_header_welcome") );
      out.write("\n");
      out.write("\t\t\t");
      out.print(com.goldhuman.auth.AuthFilter.getRemoteUser(session));
      out.write("\n");
      out.write("\t\t\t<br>\n");
      out.write("\t\t\t<a href=\"/cricket\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_cricket") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/cricket/views.html\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_views") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/role/index.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request, "include_header_roleidx") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/role/manage.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request, "include_header_rolemng") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/manage/index.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request, "include_header_mng") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<br>\n");
      out.write("\t\t\t<a href=\"/iweb/manage/advindex.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_advmng") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/include/parser.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_xml") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/headerindex.jsp\" class=\"white\">[");
      out.print( LocaleUtil2.getMessage(request,"new_iweb_entry") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"javascript:window.close()\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_logout") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/include/version.jsp\" class=\"white\">[version]</a>\n");
      out.write("\t\t</td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr class=\"shadow\">\n");
      out.write("\t\t<td height=\"4\" colspan=\"3\"></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t\t<td height=\"10\" colspan=\"3\"></td>\n");
      out.write("\t</tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("<table width=\"100%\" height=\"350\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\"><tr><td>\n");
      out.write("\n");
      out.write("<table width=\"30%\" height=\"200\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n");
      out.write("  <tr>\n");
      out.write("    <td height=\"350\">\n");

	if( success )
		out.print( LocaleUtil.getMessage(request,"role_saverole_saveok") + "&nbsp;&nbsp;&nbsp;&nbsp;" );
	else if( null == session.getAttribute("gamedb_rolebean") )
		out.print( LocaleUtil.getMessage(request,"role_saverole_savetimeout") + "&nbsp;&nbsp;&nbsp;&nbsp;" );
	else
		out.print( LocaleUtil.getMessage(request,"role_saverole_savefail") + "&nbsp;&nbsp;&nbsp;&nbsp;" );

      out.write("\n");
      out.write("    </td>\n");
      out.write("  </tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("</td></tr></table>\n");
      out.write("\n");
      out.write("<table width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n");
      out.write("  <tr>\n");
      out.write("    <td height=\"10\"></td>\n");
      out.write("  </tr>\n");
      out.write("  <tr>\n");
      out.write("    <td align=\"right\" class=\"topline\"></td>\n");
      out.write("  </tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
