/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.19
 * Generated at: 2012-03-04 01:23:02 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.svr;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.goldhuman.auth.AuthFilter;
import com.goldhuman.service.interfaces.LogInfo;
import com.goldhuman.service.interfaces.GMService;
import com.goldhuman.service.GMServiceImpl;
import org.apache.commons.logging.LogFactory;
import com.goldhuman.util.LocaleUtil;
import com.goldhuman.util.*;

public final class userrolecount_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(2);
    _jspx_dependants.put("/svr/../include/foot.jsp", Long.valueOf(1319037682000L));
    _jspx_dependants.put("/svr/../include/header.jsp", Long.valueOf(1319037682000L));
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

			String userid = "";
			String roleid = "";
			String rname = "";
			String biao = request.getParameter("biao");
			if (biao != null && biao.equals("1")) {
				userid = request.getParameter("userid");
			}
			if (biao != null && biao.equals("2")) {
				roleid = request.getParameter("roleid");
				if (roleid != null && roleid.trim().length() > 0) {
					try {
						int rid = Integer.parseInt(roleid);
						GMService gs = new GMServiceImpl();
						int tmpid = gs.roleid2Uid(new Integer(rid), new LogInfo());
						if (-1 == tmpid)
							throw new Exception("roleid2Uid error");
						userid = Integer.toString(tmpid);
					} catch (Exception e) {
						out.println(LocaleUtil.getMessage(request,"role_userrolecount_inputnum") + "&nbsp;<font color=red size=2>" + e.getMessage() + "</font>");

					}
				}
			}
			if (biao != null && biao.equals("3")) {
				rname = request.getParameter("roname");
				if (rname != null && rname.trim().length() > 0) {
					try {
						GMService gs = new GMServiceImpl();
						int tem = gs.getRoleIdByName(rname, new LogInfo());
						if (-1 == tem)
							throw new Exception("getRoleIdByName error");
						int tmpid = gs.roleid2Uid(new Integer(tem), new LogInfo());
						if (-1 == tmpid)
							throw new Exception("roleid2Uid error");
						userid = Integer.toString(tmpid);
					} catch (Exception e) {
						out.println("<font color=red size=2>" + LocaleUtil.getMessage(request,"role_userrolecount_nofound") + "</font>");
					}
				}
			}

			LogFactory.getLog("userrolecount.jsp").info(
					"userid=" + userid + "," + "operator="
							+ AuthFilter.getRemoteUser(session));

			
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
      out.write("<html>\n");
      out.write("\t<head>\n");
      out.write("\n");
      out.write("\t\t<title>");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_rolecount") );
      out.write("</title>\n");
      out.write("\t\t<link href=\"../include/style.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("\t</head>\n");
      out.write("\n");
      out.write("\t<body>\n");
      out.write("\n");
      out.write("\t\t");
      out.write("\n");
      out.write("\n");
      out.write("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n");
      out.write("\t<tr>\n");
      out.write("\t\t<td height=\"8\" colspan=\"3\" class=\"topline\"></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t\t<td width=\"225\" height=\"54\" bgcolor=\"#9E0B0E\">\n");
      out.write("\t\t\t&nbsp;\n");
      out.write("\t\t</td>\n");
      out.write("\t\t<td width=\"40\" bgcolor=\"#9E0B0E\">\n");
      out.write("\t\t\t&nbsp;\n");
      out.write("\t\t</td>\n");
      out.write("\t\t<td width=\"500\" class=\"navColor\">\n");
      out.write("\t\t\t(abroad) \n");
      out.write("\t\t\t");
      out.print( LocaleUtil.getMessage(request,"include_header_welcome") );
      out.write("\n");
      out.write("\t\t\t");
      out.print(com.goldhuman.auth.AuthFilter.getRemoteUser(session));
      out.write("\n");
      out.write("\t\t\t<br>\n");
      out.write("\t\t\t<a href=\"/cricket\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_cricket") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/cricket/views.html\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_views") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/role/index.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request, "include_header_roleidx") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/role/manage.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request, "include_header_rolemng") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/manage/index.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request, "include_header_mng") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<br>\n");
      out.write("\t\t\t<a href=\"/iweb/manage/advindex.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_advmng") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"/iweb/include/parser.jsp\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_xml") );
      out.write("]</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("\t\t\t<a href=\"javascript:window.close()\" class=\"white\">[");
      out.print( LocaleUtil.getMessage(request,"include_header_logout") );
      out.write("]</a>\n");
      out.write("\t\t</td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr class=\"shadow\">\n");
      out.write("\t\t<td height=\"4\" colspan=\"3\"></td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t\t<td height=\"10\" colspan=\"3\"></td>\n");
      out.write("\t</tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t<table width=\"100%\" height=\"514\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n");
      out.write("\n");
      out.write("\t\t\t<tr>\n");
      out.write("\t\t\t\t<td>\n");
      out.write("\t\t\t\t\t<TABLE align=\"center\" border=\"1\" cellpadding=\"0\" cellspacing=\"1\" width=\"400px\">\n");
      out.write("\t\t\t\t\t\t<TR>\n");
      out.write("\t\t\t\t\t\t\t<TH>\n");
      out.write("\t\t\t\t\t\t\t\t");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_roleid") );
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</TH>\n");
      out.write("\t\t\t\t\t\t\t<TH>\n");
      out.write("\t\t\t\t\t\t\t\t");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_rolecount") );
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</TH>\n");
      out.write("\t\t\t\t\t\t</TR>\n");
      out.write("\t\t\t\t\t\t");
LogInfo info = null;
			int uid = -1;
			String result = "";
			GMService gs = new GMServiceImpl();
			if (userid != null && userid.trim().length() > 0) {
				try {
					uid = Integer.parseInt(userid);
				} catch (Exception e) {
					 out.println( LocaleUtil.getMessage(request,"role_userrolecount_inputnum") + "&nbsp;<font color=red size=2>" + e.getMessage() + "</font>");
				}
				info = new LogInfo(uid, "", LocaleUtil.getMessage(request,"role_userrolecount_getrolecount"));
				int flag = gs.userRoleCount(new Integer(uid), info);

				switch (flag) {
				case -1:
					result = LocaleUtil.getMessage(request,"role_userrolecount_other");
					break;
				default:
					result = "" + flag;
				}

				
      out.write("\n");
      out.write("\t\t\t\t\t\t<TR>\n");
      out.write("\t\t\t\t\t\t\t<TD>\n");
      out.write("\t\t\t\t\t\t\t\t");
      out.print(uid);
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</TD>\n");
      out.write("\t\t\t\t\t\t\t<TD>\n");
      out.write("\t\t\t\t\t\t\t\t");
      out.print(result);
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</TD>\n");
      out.write("\t\t\t\t\t\t</TR>\n");
      out.write("\t\t\t\t\t\t");
}
      out.write("\n");
      out.write("\t\t\t\t\t</TABLE>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t\t</td>\n");
      out.write("\t\t\t</tr>\n");
      out.write("\n");
      out.write("\t\t\t<tr>\n");
      out.write("\t\t\t\t<td align=\"center\">\n");
      out.write("\t\t\t\t\t<form name=\"form1\" action=\"userrolecount.jsp\" method=\"post\">\n");
      out.write("\t\t\t\t\t\t");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_getrolecount") );
      out.write(":\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<table border=\"0\">\n");
      out.write("\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t<td>\n");
      out.write("\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"biao\" value=\"1\" onclick=\"show(1)\" checked=\"checked\">\n");
      out.write("\t\t\t\t\t\t\t\t\t");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_inputuserid") );
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t</td>\n");
      out.write("\t\t\t\t\t\t\t\t<td>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div id=\"uid\" style=\"display:\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"userid\" value=\"");
      out.print(userid);
      out.write("\" size=\"16\" maxlength=\"10\" />\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</td>\n");
      out.write("\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t<td>\n");
      out.write("\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"biao\" value=\"2\" onclick=\"show(2)\">\n");
      out.write("\t\t\t\t\t\t\t\t\t");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_inputroleid") );
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t</td>\n");
      out.write("\t\t\t\t\t\t\t\t<td>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div id=\"rid\" style=\"display:none\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"roleid\" value=\"");
      out.print(roleid);
      out.write("\" size=\"16\" maxlength=\"10\" />\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</td>\n");
      out.write("\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t\t\t\t<td>\n");
      out.write("\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"biao\" value=\"3\" onclick=\"show(3)\">\n");
      out.write("\t\t\t\t\t\t\t\t\t");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_inputrolename") );
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t</td>\n");
      out.write("\t\t\t\t\t\t\t\t<td>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div id=\"rname\" style=\"display:none\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"roname\" value=\"");
      out.print(rname);
      out.write("\" size=\"16\" maxlength=\"10\" />\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</td>\n");
      out.write("\t\t\t\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\t\t</table>\n");
      out.write("\t\t\t\t\t\t<input type=\"submit\" value=\"");
      out.print( LocaleUtil.getMessage(request,"role_userrolecount_submit") );
      out.write("\">\n");
      out.write("\t\t\t\t\t</form>\n");
      out.write("\t\t\t\t</td>\n");
      out.write("\t\t\t</tr>\n");
      out.write("\n");
      out.write("\t\t</table>\n");
      out.write("\t\t");
      out.write("\n");
      out.write("<table width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n");
      out.write("  <tr>\n");
      out.write("    <td height=\"10\"></td>\n");
      out.write("  </tr>\n");
      out.write("  <tr>\n");
      out.write("    <td align=\"right\" class=\"topline\"></td>\n");
      out.write("  </tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("\t\t<script language=\"javascript\">\n");
      out.write("<!--\n");
      out.write("\n");
      out.write("    function show(t){\n");
      out.write("      if(t==1)\n");
      out.write("        document.getElementById(\"uid\").style.display=\"\";\n");
      out.write("      else\n");
      out.write("        document.getElementById(\"uid\").style.display=\"none\";\n");
      out.write("      if(t==2)  \n");
      out.write("        document.getElementById(\"rid\").style.display=\"\";\n");
      out.write("      else\n");
      out.write("        document.getElementById(\"rid\").style.display=\"none\"; \n");
      out.write("      if(t==3)\n");
      out.write("        document.getElementById(\"rname\").style.display=\"\";\n");
      out.write("      else \n");
      out.write("        document.getElementById(\"rname\").style.display=\"none\";       \n");
      out.write("          \n");
      out.write("    \n");
      out.write("    }\n");
      out.write("\n");
      out.write("-->\n");
      out.write("\n");
      out.write("</script>\n");
      out.write("\t</body>\n");
      out.write("</html>\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
