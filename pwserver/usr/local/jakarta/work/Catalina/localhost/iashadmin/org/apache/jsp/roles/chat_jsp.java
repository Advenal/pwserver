/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.19
 * Generated at: 2012-03-29 05:51:44 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.roles;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import org.apache.axis.encoding.Base64;
import java.io.*;
import com.goldhuman.util.*;

public final class chat_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(4);
    _jspx_dependants.put("/include/head.jsp", Long.valueOf(1330833145000L));
    _jspx_dependants.put("/roles/.pwadminconf.jsp", Long.valueOf(1330828226000L));
    _jspx_dependants.put("/include/footer.jsp", Long.valueOf(1332996547000L));
    _jspx_dependants.put("/include/topheader.jsp", Long.valueOf(1332995893000L));
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write('\n');
      out.write('\n');

//-------------------------------------------------------------------------------------------------------------------------
//------------------------------- SETTINGS --------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

	String iweb_password = "18d87abbf43e2d4c0c0661ee65ae7197";

	String db_host = "localhost";
	String db_port = "3306";
	String db_user = "root";
	String db_password = "root123";
	String db_database = "pw";

	// Type of your Items Database required for mapping ID's to Names
	// Options are my or pwi
	String item_labels = "ms";

	// Absolute Path to your PW-Server main directory (startscript, stopscript, /gamed)
	String pw_server_path = "/PWServer/";

	// If you have hundreds of characters or heavy web acces through this site
	// It is recommend to turn the realtime character list feature off (false)
	// to prevent server from overload injected by character list generation
	boolean enable_character_list = false;

	String pw_server_name = "Your PW Servername";
	String pw_server_exp = "50.000x";
	String pw_server_sp = "50.000x";
	String pw_server_drop = "50.000x";
	String pw_server_coins = "50.000x";
	String pw_server_description = "+ Your server description here<br>+ 2 x 2.4 GHz, 2048 MB RAM, 100 MBit LAN<br>+ all dungeons open<br>+ tideborn nirvana weapons<br>+ World bosses";

//-------------------------------------------------------------------------------------------------------------------------
//----------------------------- END SETTINGS ------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

	if(request.getSession().getAttribute("items") == null)
	{
		String[] items = new String[28001];

		try
		{
			BufferedReader br = new BufferedReader(new FileReader(new File(request.getRealPath("/include/items") + "/my.dat")));
			if(item_labels.compareTo("pwi") == 0)
			{
				br = new BufferedReader(new FileReader(new File(request.getRealPath("/include/items") + "/pwi.dat")));
			}
			int count = 0;
			String line;
			while((line = br.readLine()) != null && count < 28001)
			{
				items[count] = line;
				count++;
			}
			br.close();
		}
		catch(Exception e)
		{
		}

		request.getSession().setAttribute("items", items);
	}

      out.write('\n');
      out.write('\n');

	String message = "<br>";
	boolean allowed = false;

	if(request.getSession().getAttribute("username") == null)


	
	{
		out.println("<p align=\"right\"><font color=\"#ee0000\"><b>���������������,������ ��� ����������!</b></font></p>");
	}
	else
	{
		allowed = true;
	}

      out.write('\n');
      out.write('\n');

	if(request.getParameter("process") != null && allowed)
	{
		if(request.getParameter("process").compareTo("delete") == 0)
		{
			try
			{
				FileWriter fw = new FileWriter("/PWServer/logservice/logs/world2.chat");
				fw.close();
				message = "<font color=\"#00cc00\"><b>Chat Log File Cleared</b></font>";
			}
			catch(Exception e)
			{
				message = "<font color=\"#ee0000\"><b>Clearing Chat Log File Failed</b></font>";
			}
		}

		if(request.getParameter("broadcast") != null)
		{
			String broadcast = request.getParameter("broadcast");

			if(protocol.DeliveryDB.broadcast((byte)9, -1, broadcast))
			{
				message = "<font color=\"#00cc00\"><b>Message Send</b></font>";
				try
				{
					BufferedWriter bw = new BufferedWriter(new FileWriter("/PWServer/logservice/logs/world2.chat", true));
					bw.write((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime())) + " pwserver glinkd-0: chat : Chat: src=-1 chl=9 msg=" + Base64.encode(broadcast.getBytes("UTF-16LE")) + "\n") ;
					bw.close() ;
				}
				catch(Exception e)
				{
					message += "<br><font color=\"#ee0000\">Appending Message to Log File Failed</font>";
				}
			}
			else
			{
				message = "<font color=\"#ee0000\"><b>Sending Message Failed</b></font>";
			}
		}
	}

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("\t<link rel=\"shortcut icon\" href=\"include/fav.ico\">\n");
      out.write("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"include/style.css\">\n");
      out.write('\r');
      out.write('\n');
      out.write("\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("<div align=\"center\">\n");
      out.write("\n");
      out.write("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write(" <tr><td align=\"center\"><img align=\"center\" src=\"http://1.bp.blogspot.com/-4IDvG58kMzY/Tgc7-L4wb8I/AAAAAAAACUw/5V7s9qBKIVQ/s1600/header102610genesisclas.jpg\"></td></tr>\n");
      out.write("<tr>\n");
      out.write("<td>\n");
      out.write("\t<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("\t<tr>\n");
      out.write("\t<td class=\"ver_10_black\" align=\"center\">\n");
      out.write("\t\t<a href=\"/iashadmin/roles/index.jsp\"><img src=\"/iashadmin/images/menu.png\" width=\"712\" height=\"22\" border=\"0\" usemap=\"#Map\" /></a></td>\n");
      out.write("\t</tr>\n");
      out.write("    </table>\n");
      out.write("</td></tr></table><br>\n");
      out.write("\n");
      out.write("<map name=\"Map\" id=\"Map\"><area shape=\"rect\" coords=\"584,5,634,14\" href=\"/iashadmin/logout.jsp\" /><area shape=\"rect\" coords=\"483,4,572,15\" href=\"/iashadmin/index.jsp\" /><area shape=\"rect\" coords=\"419,5,469,14\" href=\"/iashadmin/roles/chat.jsp\" /><area shape=\"rect\" coords=\"339,3,410,16\" href=\"/iashadmin/roles/serverctrl.jsp\" /><area shape=\"rect\" coords=\"267,4,330,16\" href=\"/iashadmin/server/index.jsp\" /><area shape=\"rect\" coords=\"170,4,259,15\" href=\"/iashadmin/roles/manage.jsp\" />\n");
      out.write("  <area shape=\"rect\" coords=\"76,4,157,15\" href=\"/iashadmin/roles/index.jsp\" />\n");
      out.write("</map>\n");
      out.write("\n");
      out.write("<table width=\"800\" border=\"0\" cellspacing=\"0\">\n");
      out.write("\n");
      out.write("<table width=\"100%\" height=\"100%\" cellpadding=\"2\" cellspacing=\"0\" style=\"border: 1px solid #cccccc;\">\n");
      out.write("\t<tr>\n");
      out.write("\t\t<th height=\"1\" colspan=\"2\" style=\"padding: 5;\"><b>Monitoramento de Chat</b></th>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr bgcolor=\"#f0f0f0\">\n");
      out.write("\t\t<td colspan=\"2\" align=\"center\" height=\"1\" width=\"100%\">\n");
      out.write("\t\t\t");
 out.print(message); 
      out.write("\n");
      out.write("\t\t</td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr bgcolor=\"#f0f0f0\">\n");
      out.write("\t\t<form action=\"chat.jsp?process=broadcast\" method=\"post\">\n");
      out.write("\t\t<td height=\"1\" width=\"100%\">\n");
      out.write("\t\t\t<input type=\"text\" name=\"broadcast\" value=\"\" style=\"width: 100%; text-align: left;\"></input>\n");
      out.write("\t\t</td>\n");
      out.write("\t\t<td height=\"1\">\n");
      out.write("\t\t\t<input type=\"image\" src=\"include/btn_submit.jpg\" style=\"border: 0;\"></input>\n");
      out.write("\t\t</td>\n");
      out.write("\t\t</form>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr>\n");
      out.write("\t\t<td colspan=\"2\" style=\"border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;  padding: 0;\">\n");
      out.write("\t\t\t<iframe src=\"conf-chat.jsp\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"margin: 0px;\"></iframe>\n");
      out.write("\t\t</td>\n");
      out.write("\t</tr>\n");
      out.write("\t<tr bgcolor=\"#f0f0f0\">\n");
      out.write("\t\t<td colspan=\"2\" align=\"center\" height=\"1\">\n");
      out.write("\t\t\t<a href=\"chat.jsp?process=delete\" title=\"Clear The Entire Chat Log File\"><img src=\"include/btn_delete.jpg\" style=\"border: 0;\"></img></a>\n");
      out.write("\t\t</td>\n");
      out.write("\t</tr>\n");
      out.write("</table>\n");
      out.write("</div>\n");
      out.write("<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
      out.write("<tr>\n");
      out.write("\t<td align=\"center\"><img align=\"center\" src=\"\"></td>\n");
      out.write("</tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
