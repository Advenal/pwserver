/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.19
 * Generated at: 2012-12-23 21:10:06 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.util.*;
import java.security.*;
import java.io.*;

public final class account_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


    	String pw_encode(String salt, MessageDigest alg)
	{
		alg.reset(); 
		alg.update(salt.getBytes());
		byte[] digest = alg.digest();
		StringBuffer hashedpasswd = new StringBuffer();
		String hx;
		for(int i=0; i<digest.length; i++)
		{
			hx =  Integer.toHexString(0xFF & digest[i]);
			//0x03 is equal to 0x3, but we need 0x03 for our md5sum
			if(hx.length() == 1)
			{
				hx = "0" + hx;
			} 
			hashedpasswd.append(hx);
		}
		salt = "0x" + hashedpasswd.toString();

        	return salt;
   	}

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(1);
    _jspx_dependants.put("/WEB-INF/.pwadminconf.jsp", Long.valueOf(1356261856000L));
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');

//-------------------------------------------------------------------------------------------------------------------------
//------------------------------- SETTINGS --------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

	// pwadmin access password as md5 encrypted string
	// default password is root -> 63a9f0ea7bb98050796b649e85481845
	String iweb_password = "f67e209e9b8164dcb6812b9ac4357ed1";

	// connection settings to the mysql pw database
	String db_host = "localhost";
	String db_port = "3306";
	String db_user = "root";
	String db_password = "1011";
	String db_database = "pw";

	// Type of your items database required for mapping id's to names
	// Options are my or pwi
	String item_labels = "pwi";

	// Absolute path to your PW-Server main directory (startscript, stopscript, /gamed)
	// requires a tailing slash
	String pw_server_path = "/root/";

	// If you have hundreds of characters or heavy web acces through this site
	// It is recommend to turn the realtime character list feature off (false)
	// to prevent server from overload injected by character list generation
	boolean enable_character_list = true;

	String pw_server_name = "Your PW Servername";
	String pw_server_description = "+ Your server description here<br>+ 2 x 2.4 GHz, 2048 MB RAM, 100 MBit LAN<br>+ all dungeons open<br>+ tideborn nirvana weapons<br>+ World bosses";

//-------------------------------------------------------------------------------------------------------------------------
//----------------------------- END SETTINGS ------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

	String pw_server_exp = "?";
	String pw_server_sp = "?";
	String pw_server_drop = "?";
	String pw_server_coins = "?";

	BufferedReader bfr = new BufferedReader(new FileReader(pw_server_path + "gamed/ptemplate.conf"));

	String row;
	while((row = bfr.readLine()) != null)
	{
		row = row.replaceAll("\\s", "");
		if(row.indexOf("exp_bonus=") != -1)
		{
			int pos = row.length();
			if(row.indexOf("#") != -1)
			{
				pos = row.indexOf("#");
			}
			pw_server_exp = row.substring(10, pos);
		}
		if(row.indexOf("sp_bonus=") != -1)
		{
			int pos = row.length();
			if(row.indexOf("#") != -1)
			{
				pos = row.indexOf("#");
			}
			pw_server_sp = row.substring(9, pos);
		}
		if(row.indexOf("drop_bonus=") != -1)
		{
			int pos = row.length();
			if(row.indexOf("#") != -1)
			{
				pos = row.indexOf("#");
			}
			pw_server_drop = row.substring(11, pos);
		}
		if(row.indexOf("money_bonus=") != -1)
		{
			int pos = row.length();
			if(row.indexOf("#") != -1)
			{
				pos = row.indexOf("#");
			}
			pw_server_coins = row.substring(12, pos);
		}
	}

	bfr.close();

	if(request.getSession().getAttribute("items") == null)
	{
		String[] items = new String[30001];

		try
		{
			bfr = new BufferedReader(new InputStreamReader(new FileInputStream(new File(request.getRealPath("/include/items") + "/default.dat")), "UTF8"));
			if(item_labels.compareTo("my") == 0)
			{
				bfr = new BufferedReader(new InputStreamReader(new FileInputStream(new File(request.getRealPath("/include/items") + "/my.dat")), "UTF8"));
			}
			if(item_labels.compareTo("pwi") == 0)
			{
				bfr = new BufferedReader(new InputStreamReader(new FileInputStream(new File(request.getRealPath("/include/items") + "/pwi.dat")), "UTF8"));
			}
			int count = 0;
			while((row = bfr.readLine()) != null && count < 30001)
			{
				items[count] = row;
				count++;
			}
			bfr.close();
		}
		catch(Exception e)
		{
		}

		request.getSession().setAttribute("items", items);
	}

      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');

	boolean allowed = false;

	if(request.getSession().getAttribute("ssid") == null)
	{
		out.println("<p align=\"right\"><font color=\"#ee0000\"><b>Login for Account administration...</b></font></p>");
	}
	else
	{
		allowed = true;
	}

	String message = "<br>";
	if(request.getParameter("action") != null)
	{
			String action = new String(request.getParameter("action"));

			if(action.compareTo("adduser") == 0)
			{
				String login = request.getParameter("login");
				String password = request.getParameter("password");
				String mail = request.getParameter("mail");

				if(login.length() > 0 && password.length() > 0)
				{
					if(login.length() < 4 || login.length() > 10 || password.length() < 4 || password.length() > 10)
					{
						message = "<font color=\"ee0000\">Only 4-10 Characters</font>";
					}
					else
					{
						String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
						boolean check = true;
						char c;
						for(int i=0; i<login.length(); i++)
						{
							c = login.charAt(i);
							if (alphabet.indexOf(c) == -1)
							{
								check = false;
								break;
							}
						}
						for(int i=0; i<password.length(); i++)
						{
							c = password.charAt(i);
							if (alphabet.indexOf(c) == -1)
							{
								check = false;
								break;
							}
						}

						if(!check)
						{
							message = "<font color=\"ee0000\">Forbidden Characters</font>";
						}
						else
						{
							try
							{
								Class.forName("com.mysql.jdbc.Driver").newInstance();
								Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
								Statement statement = connection.createStatement();
								ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE name='" + login + "'");
								int count = 0;

								while (rs.next())
								{
									count++;
								}
								if(count > 0)
								{
									message = "<font color=\"ee0000\">User Already Exists</font>";
								}
								else
								{
									password = pw_encode(login + password, MessageDigest.getInstance("MD5"));
									rs = statement.executeQuery("call adduser('" + login + "', " + password + ", '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '', '', " + password + ")");
									message = "<font color=\"00cc00\">Account Created</font>";
								}

								rs.close();
								statement.close();
								connection.close();
							}
							catch(Exception e)
							{
								message = "<font color=\"#ee0000\"><b>Connection to MySQL Database Failed</b></font>";
							}
						}
					}
				}
			}

			if(action.compareTo("passwd") == 0)
			{
				String login = request.getParameter("login");
				String password_old = request.getParameter("password_old");
				String password_new = request.getParameter("password_new");

				if(login.length() > 0 && password_old.length() > 0 && password_new.length() > 0)
				{
					if(password_new.length() < 4 || password_new.length() > 10)
					{
						message = "<font color=\"ee0000\">Only 4-10 Characters</font>";
					}
					else
					{
						String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
						boolean check = true;
						char c;
						for(int i=0; i<password_new.length(); i++)
						{
							c = password_new.charAt(i);
							if (alphabet.indexOf(c) == -1)
							{
								check = false;
								break;
							}
						}

						if(!check)
						{
							message = "<font color=\"ee0000\">Forbidden Characters</font>";
						}
						else
						{
							try
							{
								Class.forName("com.mysql.jdbc.Driver").newInstance();
								Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
								Statement statement = connection.createStatement();
								ResultSet rs = statement.executeQuery("SELECT ID, passwd FROM users WHERE name='" + login + "'");
								String password_stored = "";
								String id_stored = "";
								int count = 0;
								while(rs.next())
								{
									id_stored = rs.getString("ID");
									password_stored = rs.getString("passwd");
									count++;
								}

								if(count <= 0)
								{
									message = "<font color=\"ee0000\">User Don't Exists</font>";
								}
								else
								{
									password_old = pw_encode(login + password_old, MessageDigest.getInstance("MD5"));

									// Some hard encoding problems requires a strange solution...
									// changePasswd -> wrong encoding password destroyed...
									// Only a temp entry in database gives us a correct encoded password for comparsion

									rs = statement.executeQuery("call adduser('" + login + "_TEMP_USER', " + password_old + ", '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '', '', " + password_old + ")");
									rs = statement.executeQuery("SELECT passwd FROM users WHERE name='" + login + "_TEMP_USER'");
									rs.next();
									password_old = rs.getString("passwd");

									// Delete temp entry
									statement.executeUpdate("DELETE FROM users WHERE name='" + login + "_TEMP_USER'");

									if(password_old.compareTo(password_stored) != 0)
									{
										message = "<font color=\"ee0000\">Old Password Mismatch</font>";
									}
									else
									{
										password_new = pw_encode(login + password_new, MessageDigest.getInstance("MD5"));

										// LOCK TABLE to ensure that nobody else get the original ID of the user
										statement.executeUpdate("LOCK TABLE users WRITE");
										// Delete old entry
										statement.executeUpdate("DELETE FROM users WHERE name='" + login + "'");
										// Add new entry
										rs = statement.executeQuery("call adduser('" + login + "', " + password_new + ", '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0', '', '', " + password_new + ")");
										// change new entry ID to original ID - necessary to keep characters of this account
										statement.executeUpdate("UPDATE users SET ID='" + id_stored + "' WHERE name='" + login + "'");
										// UNLOCK TABLES
										statement.executeUpdate("UNLOCK TABLES");

										message = "<font color=\"00cc00\">Password Changed</font>";
									}
								}

								rs.close();
								statement.close();
								connection.close();
							}
							catch(Exception e)
							{
								message = "<font color=\"#ee0000\"><b>Connection to MySQL Database Failed</b></font>";
							}
						}
					}
				}
			}

			if(action.compareTo("deluser") == 0)
			{
				if(request.getSession().getAttribute("ssid") == null)
				{
					message = "<font color=\"ee0000\">Acces Denied</font>";
				}
				else
				{
					String type = request.getParameter("type");
					String ident = request.getParameter("ident");
					String character = request.getParameter("character");

					if(type.length() > 0 && ident.length() > 0 && character.length() > 0)
					{
						try
						{
							Class.forName("com.mysql.jdbc.Driver").newInstance();
							Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
							Statement statement = connection.createStatement();
							ResultSet rs;
							int count;

							if(type.compareTo("id") == 0)
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE ID='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									count++;
								}
							}

							else
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE name='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									ident = rs.getString("ID");
									count++;
								}
							}

							if(count <= 0)
							{
								message = "<font color=\"ee0000\">User Don't Exists</font>";
							}
							else
							{
								statement.executeUpdate("DELETE FROM users WHERE ID='" + ident + "'");
								message = "<font color=\"00cc00\">Account Deleted</font><br><font color=\"ee0000\">Please Check for Existing Characters (ID " + ident + " - " + (15+Integer.parseInt(ident)) + ")</font>";
							}

							rs.close();
							statement.close();
							connection.close();
						}
						catch(Exception e)
						{
							message = "<font color=\"#ee0000\"><b>Connection to MySQL Database Failed</b></font>";
						}
					}
				}
			}

			if(action.compareTo("changegm") == 0)
			{
				if(request.getSession().getAttribute("ssid") == null)
				{
					message = "<font color=\"ee0000\">Acces Denied</font>";
				}
				else
				{
					String type = request.getParameter("type");
					String ident = request.getParameter("ident");
					String act = request.getParameter("act");

					if(type.length() > 0 && ident.length() > 0 && act.length() > 0)
					{
						try
						{
							Class.forName("com.mysql.jdbc.Driver").newInstance();
							Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
							Statement statement = connection.createStatement();
							ResultSet rs;
							int count;

							if(type.compareTo("id") == 0)
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE ID='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									count++;
								}
							}

							else
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE name='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									ident = rs.getString("ID");
									count++;
								}
							}

							if(count <= 0)
							{
								message = "<font color=\"ee0000\">User Don't Exists</font>";
							}
							else
							{
								rs = statement.executeQuery("SELECT userid FROM auth WHERE userid='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									count++;
								}
								if(count > 0)
								{
									if(act.compareTo("delete") == 0)
									{
										statement.executeUpdate("DELETE FROM auth WHERE userid='" + ident + "'");
										message = "<font color=\"00cc00\">GM Access Removed From User</font>";
									}
									else
									{
										message = "<font color=\"ee0000\">User Already Have GM Access</font>";
									}
								}
								else
								{
									if(act.compareTo("add") == 0)
									{
										rs = statement.executeQuery("call addGM('" + ident + "', '1')");
										message = "<font color=\"00cc00\">GM Access Added For User</font>";
									}
									else
									{
										message = "<font color=\"ee0000\">User Don't Have GM Access</font>";
									}
								}
							}

							rs.close();
							statement.close();
							connection.close();
						}
						catch(Exception e)
						{
							message = "<font color=\"#ee0000\"><b>Connection to MySQL Database Failed</b></font>";
						}
					}
				}
			}

			if(action.compareTo("addcubi") == 0)
			{
				if(request.getSession().getAttribute("ssid") == null)
				{
					message = "<font color=\"ee0000\">Acces Denied</font>";
				}
				else
				{
					String type = request.getParameter("type");
					String ident = request.getParameter("ident");
					int amount = 0;
					try
					{
						amount = Integer.parseInt(request.getParameter("amount"));
					}
					catch(Exception e)
					{}

					if(type.length() > 0 && ident.length() > 0)
					{
						if(amount < 1 || amount > 999999)
						{
							message = "<font color=\"ee0000\">Invalid Amount (1-999999)</font>";
						}
						else
						{
							try
							{
								Class.forName("com.mysql.jdbc.Driver").newInstance();
								Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
								Statement statement = connection.createStatement();
								ResultSet rs;
								int count;

								if(type.compareTo("id") == 0)
								{
									rs = statement.executeQuery("SELECT ID FROM users WHERE ID='" + ident + "'");
									count = 0;
									while(rs.next())
									{
										count++;
									}
								}

								else
								{
									rs = statement.executeQuery("SELECT ID FROM users WHERE name='" + ident + "'");
									count = 0;
									while(rs.next())
									{
										ident = rs.getString("ID");
										count++;
									}
								}

								if(count <= 0)
								{
									message = "<font color=\"ee0000\">User Don't Exists</font>";
								}
								else
								{
									rs = statement.executeQuery("call usecash ( '" + ident + "' , 1, 0, 1, 0, '" + 100*amount + "', 1, @error)");
									message = "<font color=\"00cc00\">" + amount + ".00 Cubi Gold Added</font><br><font color=\"ee0000\">Transaction May Take Up To 5 Minutes<br>Relog Required To Receive Cubi</font>";
								}
								
								rs.close();
								statement.close();
								connection.close();
							}
							catch(Exception e)
							{
								message = "<font color=\"#ee0000\"><b>Connection to MySQL Database Failed</b></font>";
							}
						}
					}
				}
			}
	}

      out.write("\n");
      out.write("\n");
      out.write("<table width=\"800\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n");
      out.write("\n");
      out.write("<tr>\n");
      out.write("\t<td height=\"1\" align=\"center\" valign=\"top\" colspan=\"3\">\n");
      out.write("\t\t<b>");
 out.print(message); 
      out.write("</b>\n");
      out.write("\t</td>\n");
      out.write("</tr>\n");
      out.write("\n");
      out.write("<tr>\n");
      out.write("\t<td height=\"1\" align=\"center\" valign=\"top\" colspan=\"3\">\n");
      out.write("\t\t<br>\n");
      out.write("\t</td>\n");
      out.write("</tr>\n");
      out.write("\n");
      out.write("<tr>\n");
      out.write("\t<td align=\"center\" valign=\"top\">\n");
      out.write("\t\t<form action=\"index.jsp?page=account&action=adduser\" method=\"post\" style=\"margin: 0px;\">\n");
      out.write("\t\t\t<table width=\"240\" cellpadding=\"5\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<th align=\"center\" colspan=\"2\">\n");
      out.write("\t\t\t\t\t\t<b><font color=\"#ffffff\">ACCOUNT REGISTRATION</font></b>\n");
      out.write("\t\t\t\t\t</th>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td>Login:</td><td align=\"right\"><input type=\"text\" name=\"login\" style=\"width: 100; text-align: center;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td>Password:</td><td align=\"right\"><input type=\"password\" name=\"password\" style=\"width: 100; text-align: center;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td>E-Mail</td><td align=\"right\"><input type=\"text\" name=\"mail\" value=\"NOT NEEDED\" style=\"width: 100; text-align: center;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td align=\"center\" colspan=\"2\"><input type=\"image\" name=\"submit\" src=\"include/btn_register.jpg\" style=\"border: 0px;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t</table>\n");
      out.write("\t\t</form>\n");
      out.write("\t</td>\n");
      out.write("\n");
      out.write("\t<td align=\"center\" valign=\"top\">\n");
      out.write("\t\t<form action=\"index.jsp?page=account&action=passwd\" method=\"post\" style=\"margin: 0px;\">\n");
      out.write("\t\t\t<table width=\"240\" cellpadding=\"5\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<th align=\"center\" colspan=\"2\">\n");
      out.write("\t\t\t\t\t\t<b><font color=\"#ffffff\">CHANGE ACCOUNT PASSWORD</font></b>\n");
      out.write("\t\t\t\t\t</th>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td>Login:</td><td align=\"right\"><input type=\"text\" name=\"login\" style=\"width: 100; text-align: center;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td>Old Password:</td><td align=\"right\"><input type=\"password\" name=\"password_old\" style=\"width: 100; text-align: center;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td>New Password:</td><td align=\"right\"><input type=\"password\" name=\"password_new\" style=\"width: 100; text-align: center;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t\t<td align=\"center\" colspan=\"2\"><input type=\"image\" name=\"submit\" src=\"include/btn_change.jpg\" style=\"border: 0px;\"></td>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t</table>\n");
      out.write("\t\t</form>\n");
      out.write("\t</td>\n");
      out.write("\n");
      out.write("\t<td align=\"center\" valign=\"top\">\n");
      out.write("\t\t");

			if(allowed)
			{
				out.println("<form action=\"index.jsp?page=account&action=deluser\" method=\"post\" style=\"margin: 0px;\"><table width=\"240\" cellpadding=\"5\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">");
				out.println("<tr><th align=\"center\" colspan=\"2\"><b><font color=\"#ffffff\">DELETE ACCOUNT</font></b></th></tr>");
				out.println("<tr><td>Type:</td><td align=\"right\"><select name=\"type\" style=\"width: 100; text-align: center;\"><option value=\"id\">by ID</option><option value=\"login\">by Login</option></select></td></tr>");
				out.println("<tr><td>Identifier:</td><td align=\"right\"><input type=\"text\" name=\"ident\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td>Characters:</td><td align=\"right\"><select name=\"character\" style=\"width: 100; text-align: center;\"><option value=\"keep\">Keep</option></select></td></tr>");
				out.println("<tr><td align=\"center\" colspan=\"2\"><input type=\"image\" name=\"submit\" src=\"include/btn_delete.jpg\" style=\"border: 0px;\"></td></tr>");
				out.println("</table></form>");
			}
		
      out.write("\n");
      out.write("\t</td>\n");
      out.write("</tr>\n");
      out.write("\n");
      out.write("<tr>\n");
      out.write("\t<td height=\"1\" colspan=\"3\">\n");
      out.write("\t\t<br>\n");
      out.write("\t</td>\n");
      out.write("</tr>\n");
      out.write("\n");
      out.write("<tr>\n");
      out.write("\t<td align=\"center\" valign=\"top\">\n");
      out.write("\t\t");

			if(allowed)
			{
				out.println("<form action=\"index.jsp?page=account&action=changegm\" method=\"post\" style=\"margin: 0px;\"><table width=\"240\" cellpadding=\"5\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">");
				out.println("<tr><th align=\"center\" colspan=\"2\"><b><font color=\"#ffffff\">GAME MASTER</font></b></th></tr>");
				out.println("<tr><td>Type:</td><td align=\"right\"><select name=\"type\" style=\"width: 100; text-align: center;\"><option value=\"id\">by ID</option><option value=\"login\">by Login</option></select></td></tr>");
				out.println("<tr><td>Identifier:</td><td align=\"right\"><input type=\"text\" name=\"ident\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td>Action:</td><td align=\"right\"><select name=\"act\" style=\"width: 100; text-align: center;\"><option value=\"add\">Grant GM</option><option value=\"delete\">Deny GM</option></select></td></tr>");
				out.println("<tr><td align=\"center\" colspan=\"2\"><input type=\"image\" name=\"submit\" src=\"include/btn_submit.jpg\" style=\"border: 0px;\"></td></tr>");
				out.println("</table></form>");
			}
		
      out.write("\n");
      out.write("\t</td>\n");
      out.write("\n");
      out.write("\t<td align=\"center\" valign=\"top\">\n");
      out.write("\t\t");

			if(allowed)
			{
				out.println("<form action=\"index.jsp?page=account&action=addcubi\" method=\"post\" style=\"margin: 0px;\"><table width=\"240\" cellpadding=\"5\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">");
				out.println("<tr><th align=\"center\" colspan=\"2\"><b><font color=\"#ffffff\">CUBI TRANSFER</font></b></th></tr>");
				out.println("<tr><td>Type:</td><td align=\"right\"><select name=\"type\" style=\"width: 100; text-align: center;\"><option value=\"id\">by ID</option><option value=\"login\">by Login</option></select></td></tr>");
				out.println("<tr><td>Identifier:</td><td align=\"right\"><input type=\"text\" name=\"ident\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td>Amount:</td><td align=\"right\"><input type=\"text\" name=\"amount\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td align=\"center\" colspan=\"2\"><input type=\"image\" name=\"submit\" src=\"include/btn_submit.jpg\" style=\"border: 0px;\"></td></tr>");
				out.println("</table></form>");
			}
		
      out.write("\n");
      out.write("\t</td>\n");
      out.write("\n");
      out.write("\t<td align=\"center\" valign=\"top\">\n");
      out.write("\t\t");

			if(allowed)
			{
				out.println("<table cellpadding=\"0\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">");
				out.println("<tr><th height=\"1\" align=\"center\" colspan=\"3\" style=\"padding: 5;\"><b><font color=\"#ffffff\">BROWSE ACCOUNTS</font></b></th></tr>");
				out.println("<tr bgcolor=\"f0f0f0\"><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><b>ID</b></td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><b>Name</b></td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><b>Creation Time</b></td></tr>");
				out.println("<tr><td colspan=\"3\" ><div style=\"width: 240; height: 96; overflow: auto;\">");
				out.println("<table width=\"100%\" cellpadding=\"3\" cellspacing=\"0\" style=\"border:0px solid #cccccc;\">");

				try
				{
					Class.forName("com.mysql.jdbc.Driver").newInstance();
					Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
					Statement statement = connection.createStatement();
					ResultSet rs;

					rs = statement.executeQuery("SELECT DISTINCT userid FROM auth");
					ArrayList gm = new ArrayList();

					while(rs.next())
					{
						gm.add(rs.getInt("userid"));
					}

					rs = statement.executeQuery("SELECT ID, name, creatime FROM users");
					while(rs.next())
					{
						if(gm.contains(rs.getInt("ID")))
						{
							out.print("<tr><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("ID") + "</td><td style=\"border-bottom: 1px solid #cccccc;\"><font color=\"#ee0000\">" + rs.getString("name") + "</font></td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("creatime").substring(0, 16) + "</td></tr>");
						}
						else
						{
							out.print("<tr><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("ID") + "</td><td style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("name") + "</td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("creatime").substring(0, 16) + "</td></tr>");
						}
					}

					rs.close();
					statement.close();
					connection.close();
				}
				catch(Exception e)
				{
					out.println("<tr><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><font color=\"#ee0000\"><b>Connection to MySQL Database Failed</b></font></table></div></td></tr>");
				}

				out.println("</table></div></td></tr></table>");
			}
		
      out.write("\n");
      out.write("\t</td>\n");
      out.write("</tr>\n");
      out.write("\n");
      out.write("</table>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
