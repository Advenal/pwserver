<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%
	boolean showTag = false;
	if(request.getSession().getAttribute("username")!=null) showTag = true;
%>
<html>
<head>
<%@include file="/include/head.jsp"%>  
<script language=javascript>
function onqueryrolexml()
{
	document.myquery.action = "modrolexml.jsp";
	
	document.myquery.submit()
	return true;
}
</script>
<style type="text/css">
.ver_12_black_b {
	text-align: center;
}
</style>
</head>
<body>
<%if(showTag)
{%>
<%@include file="/include/topheader.jsp"%>  
<table width="800" border="0" align="center" cellspacing="0">
	<tr><td colspan="2">&nbsp;</td></tr> 
	<tr>
	  <td colspan="2" class="ver_12_black_b">Gerenciar Personagem</td></tr> 
	<tr>
		<td colspan="2" align="center">
			<table width="95%" cellspacing="0" cellpadding="0">
				<tr><td>&nbsp;</td></tr>
				<tr>
				  <td class="ver_10_black">Personagens padrão:</td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=16">Guerreiro</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=17">Mistico</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=18">Arcano</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=19">Mago</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=27">Mercenario</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=31">Sacerdote</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=28">Arqueiro</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=20">Espiritualista</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=24">Barbaro</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrolexml.jsp?roleid=23">Feiticeira</a></td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td class="ver_10_black">&nbsp;<br>
						<form action="modrole.jsp" name="myquery" method="post">
							<table width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan=2">&nbsp;</td></tr>
								<tr>
								  <td colspan=2" class="ver_10_black">Selecionar um personagem especifico:</td></tr>
								<tr><td colspan=2">&nbsp;</td></tr>              
								<tr>
									<td width="10%" class="ver_10_black">Por ID :</td>
									<td width="90%"><input type="text" name="roleid" value="" size="16" maxlength="10"></td>        
								</tr>
								<tr>
									<td class="ver_10_black">Por Nome:</td>
									<td><input type="text" name="name" value="" size="20" maxlength="64"></td>        
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" value="Nao funciona use o de baixo" class="button"></td>
								</tr>
								<tr>
									<td></td>
									<td class="ver_10_black"><a href="javascript:onqueryrolexml();">Vizualizar XML</a></td>        
								</tr>       
							</table>
						</form>
					</td>
				</tr>   
			</table>
		</td>
	</tr> 
</table><%
} else
{%>
<%@include file="/include/topheader_nologin.jsp"%><%
} %>
<%@include file="/include/footer.jsp"%>
</body>
</html> 
