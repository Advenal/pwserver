<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="java.security.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>
<%@page import="java.lang.*"%>
<%@page import="java.util.Calendar.*"%>
<%@page import="com.goldhuman.util.*" %>
<%@page import="protocol.*"%>
<%@page import="com.goldhuman.auth.*"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>
<%@include file="/include/.config.jsp"%>

<html>
<head>
<link href="../iweb/include/style.css" rel="stylesheet" type="text/css">
<title>Управление аккаунтами</title>
<%@include file="/include/header.jsp"%>
</head>
<body style="background-color:#ffffff;">
<div align="center"><font size="5">Управление аккаунтами</font>
<table width="800" border="0" cellspacing="0">

<%
	String message = "<br>";
	if(request.getParameter("action") != null)
	{
			String action = new String(request.getParameter("action"));


			if(action.compareTo("changegm") == 0)
			{
					String type = request.getParameter("type");
					String ident = request.getParameter("ident");
					String truename = request.getParameter("truename");
					String act = request.getParameter("act");

					if(type.length() > 0 && ident.length() > 0 && act.length() > 0)
					{
						try
						{
							Class.forName("com.mysql.jdbc.Driver").newInstance();
							Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
							Statement statement = connection.createStatement();
							ResultSet rs;
							int count;

							if(type.compareTo("id") == 0)
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE ID='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									count++;
								}
							}

							else
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE name='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									ident = rs.getString("ID");
									count++;
								}
							}

							if(count <= 0)
							{
								message = "<font color=\"ee0000\">Пользователя не существует</font>";
							}
							else
							{
								rs = statement.executeQuery("SELECT userid FROM auth WHERE userid='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									count++;
								}
								if(count > 0)
								{
									if(act.compareTo("delete") == 0)
									{
										statement.executeUpdate("DELETE FROM auth WHERE userid='" + ident + "'");
										message = "<font color=\"00cc00\">Права GM сняты с пользователя</font>";
									}
									else
									{
										message = "<font color=\"ee0000\">Пользователь Уже GM</font>";
									}
								}
								else
								{
									if(act.compareTo("add") == 0)
									{
										rs = statement.executeQuery("call addGM('" + ident + "', '1')");
										message = "<font color=\"00cc00\">Права GM выданы пользователю</font>";
									}
									else
									{
										message = "<font color=\"ee0000\">Доступ к GM Недоступен</font>";
									}
								}
							}

							rs.close();
							statement.close();
							connection.close();
						}
						catch(Exception e)
						{
							message = "<font color=\"#ee0000\"><b>Подключиться к базе данных MySQL не удалось</b></font>";
						}
					}
					else ;
			}
			else ;

			if(action.compareTo("addcubi") == 0)
			{
					String type = request.getParameter("type");
					String ident = request.getParameter("ident");
					int amount = 0;
					try
					{
						amount = Integer.parseInt(request.getParameter("amount"));
					}
					catch(Exception e)
					{}

					if(type.length() > 0 && ident.length() > 0)
					{
						if(amount < 1 || amount > 999999999)
						{
							message = "<font color=\"ee0000\">Неверное количество (1-999999999)</font>";
						}
						else
						{
							try
							{
								Class.forName("com.mysql.jdbc.Driver").newInstance();
								Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
								Statement statement = connection.createStatement();
								ResultSet rs;
								int count;

								if(type.compareTo("id") == 0)
								{
									rs = statement.executeQuery("SELECT ID FROM users WHERE ID='" + ident + "'");
									count = 0;
									while(rs.next())
									{
										count++;
									}
								}

								else
								{
									rs = statement.executeQuery("SELECT ID FROM users WHERE name='" + ident + "'");
									count = 0;
									while(rs.next())
									{
										ident = rs.getString("ID");
										count++;
									}
								}

								if(count <= 0)
								{
									message = "<font color=\"ee0000\">Пользователь не существует</font>";
								}
								else
								{
									rs = statement.executeQuery("CALL usecash ( '" + ident + "' , 1, 0, 1, 0, '" + 100*amount + "', 1, @error)");
									message = "<font color=\"00cc00\">" + amount + ".00 GOLD Добавлено</font><br><font color=\"ee0000\">Транзакция может занять до 5 минут<br>Relog, необходимые для получения GOLD</font>";
								}
								
								rs.close();
								statement.close();
								connection.close();
							}
							catch(Exception e)
							{
								message = "<font color=\"#ee0000\"><b>Подключиться к базе данных MySQL не удалось</b></font>";
							}
						}
					}
					else ;
			}
			else ;
			
			if(action.compareTo("enhance") == 0)
			{
					String type = request.getParameter("type");
					String ident = request.getParameter("ident");
					String truename = request.getParameter("truename");
					String act = request.getParameter("act");

					if(type.length() > 0 && ident.length() > 0 && act.length() > 0)
					{
						try
						{
							Class.forName("com.mysql.jdbc.Driver").newInstance();
							Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
							Statement statement = connection.createStatement();
							ResultSet rs;
							int count;

							if(type.compareTo("id") == 0)
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE ID='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									count++;
								}
							}

							else
							{
								rs = statement.executeQuery("SELECT ID FROM users WHERE name='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									ident = rs.getString("ID");
									count++;
								}
							}

							if(count <= 0)
							{
								message = "<font color=\"ee0000\">User Don't Exists</font>";
							}
							else
							{
								rs = statement.executeQuery("SELECT userid FROM enhanced WHERE userid='" + ident + "'");
								count = 0;
								while(rs.next())
								{
									count++;
								}
								if(count > 0)
								{
									if(act.compareTo("delete") == 0)
									{
										statement.executeUpdate("DELETE FROM enhanced WHERE userid='" + ident + "'");
										message = "<font color=\"00cc00\">Enhancement Removed From User</font>";
									}
									else
									{
										message = "<font color=\"ee0000\">User is Already enhanced</font>";
									}
								}
								else
								{
									if(act.compareTo("farmer") == 0)
									{
										rs = statement.executeQuery("CALL addEnhanced('" + ident + "', '" + truename + "', '2')");
										message = "<font color=\"00cc00\">User has been enhanced to farmer</font>";
									}
									if(act.compareTo("soldier") == 0)
									{
										rs = statement.executeQuery("CALL addEnhanced('" + ident + "', '" + truename + "', '3')");
										message = "<font color=\"00cc00\">User has been enhanced to Soldier</font>";
									}
									if(act.compareTo("merchant") == 0)
									{
										rs = statement.executeQuery("CALL addEnhanced('" + ident + "', '" + truename + "', '4')");
										message = "<font color=\"00cc00\">User has been enhanced to Merchant</font>";
									}
									if(act.compareTo("hero") == 0)
									{
										rs = statement.executeQuery("CALL addEnhanced('" + ident + "', '" + truename + "', '5')");
										message = "<font color=\"00cc00\">User has been enhanced to Hero</font>";
									}
									else
									{
										message = "<font color=\"ee0000\">User is not enhanced</font>";
									}
								}
							}

							rs.close();
							statement.close();
							connection.close();
						}
						catch(Exception e)
						{
							message = "<font color=\"#ee0000\"><b>Подключиться к базе данных MySQL не удалось</b></font>";
						}
					}
					else ;
			}
			else ;
	}
	else ;
%>

<table width="800" cellpadding="0" cellspacing="0" border="0">

<tr>
	<td height="1" align="center" valign="top" colspan="3">
		<b><% out.print(message); %></b>
	</td>
</tr>

<tr>
	<td height="1" align="center" valign="top" colspan="3">
		<br>
	</td>
</tr>

<tr>
	<td height="1" colspan="3">
		<br>
	</td>
</tr>

<tr>
	<td align="center" valign="top">
		<%
				out.println("<form action=\"index.jsp?page=account&action=changegm\" method=\"post\" style=\"margin: 0px;\"><table width=\"240\" cellpadding=\"5\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">");
				out.println("<tr><th align=\"center\" colspan=\"2\"><b><font color=\"#000000\">ИГРОВОЙ MASTER</font></b></th></tr>");
				out.println("<tr><td>Тип:</td><td align=\"right\"><select name=\"type\" style=\"width: 100; text-align: center;\"><option value=\"id\">ID</option><option value=\"login\">Login</option></select></td></tr>");
				out.println("<tr><td>Аккаунт (ID):</td><td align=\"right\"><input type=\"text\" name=\"ident\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td>Имя персонажа:</td><td align=\"right\"><input type=\"text\" name=\"truename\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td>Действие:</td><td align=\"right\"><select name=\"act\" style=\"width: 100; text-align: center;\"><option value=\"add\">Дать GM</option><option value=\"delete\">Убрать GM</option></select></td></tr>");
				out.println("<tr><td align=\"center\" colspan=\"2\"><input type=\"image\" name=\"submit\" src=\"include/btn_submit.jpg\" style=\"border: 0px;\"></td></tr>");
				out.println("</table></form>");
		%>
	</td>

	<td align="center" valign="top">
		<%
				out.println("<form action=\"index.jsp?page=account&action=addcubi\" method=\"post\" style=\"margin: 0px;\"><table width=\"240\" cellpadding=\"5\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">");
				out.println("<tr><th align=\"center\" colspan=\"2\"><b><font color=\"#000000\">ЗАЧИСЛИТЬ GOLD</font></b></th></tr>");
				out.println("<tr><td>Тип:</td><td align=\"right\"><select name=\"type\" style=\"width: 100; text-align: center;\"><option value=\"id\">ID</option><option value=\"login\">Login</option></select></td></tr>");
				out.println("<tr><td>Аккаунт (ID):</td><td align=\"right\"><input type=\"text\" name=\"ident\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td>Количество:</td><td align=\"right\"><input type=\"text\" name=\"amount\" style=\"width: 100; text-align: center;\"></td></tr>");
				out.println("<tr><td align=\"center\" colspan=\"2\"><input type=\"image\" name=\"submit\" src=\"include/btn_submit.jpg\" style=\"border: 0px;\"></td></tr>");
				out.println("</table></form>");
		%>
	</td>

	<td align="center" valign="top">
		<%
				out.println("<table cellpadding=\"0\" cellspacing=\"0\" style=\"border:1px solid #cccccc;\">");
				out.println("<tr><th height=\"1\" align=\"center\" colspan=\"3\" style=\"padding: 5;\"><b><font color=\"#000000\">СПИСОК АККАУНТА</font></b></th></tr>");
				out.println("<tr bgcolor=\"f0f0f0\"><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><b>ID</b></td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><b>Имя</b></td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><b>Время создания</b></td></tr>");
				out.println("<tr><td colspan=\"3\" ><div style=\"width: 280; height: 110; overflow: auto;\">");
				out.println("<table width=\"100%\" cellpadding=\"3\" cellspacing=\"0\" style=\"border:0px solid #cccccc;\">");

				try
				{
					Class.forName("com.mysql.jdbc.Driver").newInstance();
					Connection connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/" + db_database, db_user, db_password);
					Statement statement = connection.createStatement();
					ResultSet rs;

					rs = statement.executeQuery("SELECT DISTINCT userid FROM auth");
					ArrayList gm = new ArrayList();

					while(rs.next())
					{
						gm.add(rs.getInt("userid"));
					}

					rs = statement.executeQuery("SELECT ID, name, creatime FROM users");
					while(rs.next())
					{
						if(gm.contains(rs.getInt("ID")))
						{
							out.print("<tr><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("ID") + "</td><td style=\"border-bottom: 1px solid #cccccc;\"><font color=\"#ee0000\">" + rs.getString("name") + "</font></td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("creatime").substring(0, 16) + "</td></tr>");
						}
						else
						{
							out.print("<tr><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("ID") + "</td><td style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("name") + "</td><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\">" + rs.getString("creatime").substring(0, 16) + "</td></tr>");
						}
					}

					rs.close();
					statement.close();
					connection.close();
				}
				catch(Exception e)
				{
					out.println("<tr><td align=\"center\" style=\"border-bottom: 1px solid #cccccc;\"><font color=\"#ee0000\"><b>Подключиться к базе данных MySQL не удалось</b></font></table></div></td></tr>");
				}

				out.println("</table></div></td></tr></table>");
		%>
	</td>
</tr>
</table>
</div>
</body>
<%@include file="../include/foot.jsp"%>
</html>

