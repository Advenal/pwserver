<%@page import="java.io.*"%>

<%
//-------------------------------------------------------------------------------------------------------------------------
//------------------------------- SETTINGS --------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

	String iweb_password = "18d87abbf43e2d4c0c0661ee65ae7197";

	String db_host = "localhost";
	String db_port = "3306";
	String db_user = "root";
	String db_password = "1011";
	String db_database = "pw";

	// Type of your Items Database required for mapping ID's to Names
	// Options are my or pwi
	String item_labels = "pwi";

	// Absolute Path to your PW-Server main directory (startscript, stopscript, /gamed)
	String pw_server_path = "/root/";

	// If you have hundreds of characters or heavy web acces through this site
	// It is recommend to turn the realtime character list feature off (false)
	// to prevent server from overload injected by character list generation
	boolean enable_character_list = false;

	String pw_server_name = "pw";
	String pw_server_exp = "1x";
	String pw_server_sp = "1x";
	String pw_server_drop = "1x";
	String pw_server_coins = "1x";
	String pw_server_description = "pw";

//-------------------------------------------------------------------------------------------------------------------------
//----------------------------- END SETTINGS ------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

	if(request.getSession().getAttribute("items") == null)
	{
		String[] items = new String[28001];

		try
		{
			BufferedReader br = new BufferedReader(new FileReader(new File(request.getRealPath("/include/items") + "/my.dat")));
			if(item_labels.compareTo("pwi") == 0)
			{
				br = new BufferedReader(new FileReader(new File(request.getRealPath("/include/items") + "/pwi.dat")));
			}
			int count = 0;
			String line;
			while((line = br.readLine()) != null && count < 28001)
			{
				items[count] = line;
				count++;
			}
			br.close();
		}
		catch(Exception e)
		{
		}

		request.getSession().setAttribute("items", items);
	}
%>