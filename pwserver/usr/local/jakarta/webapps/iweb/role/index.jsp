<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.*"%>
<%@page import="com.goldhuman.util.LocaleUtil"%>
<html>
<head>
<link href="../css/style1.css" rel="stylesheet" type="text/css">
<title>Управление персонажами</title>
<script language=javascript>
function onqueryrolexml()
{
	document.myquery.action = "modrolexml.jsp";
	document.myquery.submit()
	return true;
}
</script>
<%@include file="/include/header.jsp"%>
</head>
<body style="background-color:#ffffff;">
<div align="center"><font size="4">Редактирование XML</font>
<table width="100%" height="350"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td>
<table width="550" height="200" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td><b>Просмотр и редактирование стандартных персонажей:</b></td>
</tr>
<tr>
<td><hr></td>
</tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=16">Люди - Воин</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=19">Люди - Маг</a></td></tr>
				<tr>
<td><hr></td>
</tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=28">Сиды - Лучник</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=31">Сиды - Жрец</a></td></tr>
				<tr>
<td><hr></td>
</tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=23">Зооморфы - Друид</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=24">Зооморфы - Оборотень</a></td></tr>
				<tr>
<td><hr></td>
</tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=27">Амфибии - Убийца</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=20">Амфибии - Шаман</a></td></tr>
				<tr>
<td><hr></td>
</tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=18">Древние - Страж</a></td></tr>
				<tr><td class="ver_10_black"><a href="modrole.jsp?roleid=17">Древние - Мистик</a></td></tr>
				<tr>
<td><hr></td>
</tr>
<td class="ver_10_black"><br>
<form action="modrole.jsp" name="myquery" method="post">
<table width="100%" cellspacing="0" cellpadding="0">
<tr></tr>
<tr><td colspan=2" class="ver_10_black">Просмотр конкретного персонажа:</td></tr>
<tr></tr>
<tr>
<td width="15%" class="ver_10_black">Введите ID персонажа:</td>
<td width="85%"><input type="text" name="roleid" value="" size="16" maxlength="10"></td>        
</tr>
<tr>
<td class="ver_10_black">Введите имя персонажа:</td>
<td><input type="text" name="name" value="" size="20" maxlength="64"></td>        
</tr>
<tr>
<td></td>
<td><input type="submit" value="Стандартная информация о персонаже" class="button"></td>
</tr>
<tr>
<td></td>
<td class="ver_10_black"><a href="javascript:onqueryrolexml();">XML персонажа</a></td>        
</tr>       
</table>
</form>
</td>
</table>
</td>
</tr>
</table>
</div>
</body>
<%@include file="../include/foot.jsp"%>
</html>

