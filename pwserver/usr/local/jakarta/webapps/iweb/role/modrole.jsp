<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="protocol.*"%>
<%@page import="com.goldhuman.auth.*"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="com.goldhuman.util.LocaleUtil"%>
<html>
<head>
<link href="../include/style.css" rel="stylesheet" type="text/css">
<title>Редактирование стандартных персонажей</title>
</head>
<body>
<%
com.goldhuman.Common.Octets.setDefaultCharset("UTF-16LE");
String strRoleId = request.getParameter("roleid");
String strRoleName = request.getParameter("name");

LogFactory.getLog("modrole.jsp").info("request for roleid=" + strRoleId + ",name=" + strRoleName + ",operator=" + AuthFilter.getRemoteUser(session) );

int roleid = -1;
if( null != strRoleId && strRoleId.length() > 0 )
	roleid = Integer.parseInt(request.getParameter("roleid"));

if( -1 == roleid && null != strRoleName && strRoleName.length() > 0 )
{
	try{ roleid = GameDB.getRoleIdByName( strRoleName ); }
	catch (Exception e) { out.println(e.toString()); return; }
	if( -1 == roleid )
	{
		out.println(LocaleUtil.getMessage(request,"role_modrole_notsearchrole") + strRoleName + LocaleUtil.getMessage(request,"role_modrole_noroleortimeout") );
		return;
	}
}

if( -1 == roleid )
{
	out.println(LocaleUtil.getMessage(request,"role_modrole_inputidorname"));
	return;
}
RoleBean role = null;
try{
	role = GameDB.get( roleid );
	session.setAttribute( "gamedb_rolebean", role );
}
catch (Exception e)
{       
	        LogFactory.getLog("modrole.jsp").info(", GameDB.get roleid=" + roleid  + e);
		        out.println("roleid=" + roleid + e.toString()); return;
}
if (null == role)
{
	out.println(LocaleUtil.getMessage(request,"role_modrole_retry"));
	return;
}
LogFactory.getLog("modrole.jsp").info("getRoleInfo, "+role.getLogString()+",operator=" + AuthFilter.getRemoteUser(session) );
String rolename;
switch( roleid )
{
	case 16:    rolename = LocaleUtil.getMessage(request,"Базовый.Воин - мужчина");	break;
	case 17:    rolename = LocaleUtil.getMessage(request,"Базовый.Воин - женщина");	 break;
	case 18:    rolename = LocaleUtil.getMessage(request,"Базовый.Маг - мужчина");	break;
	case 19:    rolename = LocaleUtil.getMessage(request,"Базовый.Маг - женщина");	break;
	case 20:    rolename = LocaleUtil.getMessage(request,"Базовый.modrole_senlvnan");	break;
	case 21:    rolename = LocaleUtil.getMessage(request,"Базовый.modrole_senlvnv");	break;
	case 22:    rolename = LocaleUtil.getMessage(request,"Базовый.modrole_yaojinnan");	break;
	case 23:    rolename = LocaleUtil.getMessage(request,"Базовый.Друид - женщина");	break;
	case 24:    rolename = LocaleUtil.getMessage(request,"Базовый.Оборотень - мужчина");	break;
	case 25:    rolename = LocaleUtil.getMessage(request,"Базовый.modrole_souyaonv");	break;
	case 26:    rolename = LocaleUtil.getMessage(request,"Базовый.modrole_meilinnan");	break;
	case 27:    rolename = LocaleUtil.getMessage(request,"Базовый.modrole_meilinv");	break;
	case 28:    rolename = LocaleUtil.getMessage(request,"Лучник - мужчина");	break;
	case 29:    rolename = LocaleUtil.getMessage(request,"Базовый.Лучник - женщина");	break;
	case 30:    rolename = LocaleUtil.getMessage(request,"Базовый.Жрец - мужчина");	break;
	case 31:    rolename = LocaleUtil.getMessage(request,"Базовый.Жрец - женщина");	break;
	default:    rolename = LocaleUtil.getMessage(request,"Игровой персонаж");
}
%>
<%@include file="../include/header.jsp"%>
<CENTER>
	<table width="800" border="0" cellspacing="0" bgcolor="#1e90ff>
<tr><td colspan="2">&nbsp;</td></tr> 
	<tr>
<td colspan="2">
<form action="saverole.jsp" method="post">
<input type="hidden" name="roleid" value="<%=roleid%>">
<table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
<td height="30" class="ver_14_white"><b><%=rolename%>&nbsp;(roleid=<%=roleid%>)</b>&nbsp;&nbsp;--&nbsp;Информация о персонаже</td>
		</tr>
		<tr>
		<td>
	<table width="100%" cellspacing="1" cellpadding="3" bgcolor="#000000">
		<tr bgcolor="#FFFFFF">
<td width="25%" class="ver_10_black">Имя&nbsp;&nbsp;<%=StringEscapeUtils.escapeHtml(role.base.name.getString())%></td>
<td width="25%" class="ver_10_black">Раса&nbsp;&nbsp;<%=role.base.race%></td>
<td width="25%" class="ver_10_black">Профессия&nbsp;&nbsp;<%=RoleBean.ClsName(role.base.cls)%></td>
<td width="25%" class="ver_10_black">Пол&nbsp;&nbsp;<%=RoleBean.GenderName(role.base.gender)%></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Статус&nbsp;&nbsp;&nbsp;&nbsp;<%=RoleBean.StatusName(role.base.status)%></td>
<td class="ver_10_black">Время удаления&nbsp;&nbsp;&nbsp;&nbsp;<%=role.base.delete_time<=0 ? "-" : (new SimpleDateFormat("y/M/d H:m:s")).format(new Date(1000*(long)role.base.delete_time))%></td>
<td class="ver_10_black">Дата создания&nbsp;&nbsp;&nbsp;&nbsp;<%=role.base.create_time<=0 ? "-" : (new SimpleDateFormat("y/M/d H:m:s")).format(new Date(1000*(long)role.base.create_time))%></td>
<td class="ver_10_black">Дата последнего входа&nbsp;&nbsp;&nbsp;&nbsp;<%=role.base.lastlogin_time<=0 ? "-" : (new SimpleDateFormat("y/M/d H:m:s")).format(new Date(1000*(long)role.base.lastlogin_time))%></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Мир&nbsp;&nbsp;&nbsp;&nbsp;<%=role.status.worldtag%></td>
<td class="ver_10_black">Статус красного ника(ПК)&nbsp;&nbsp;&nbsp;&nbsp;<%=role.status.invader_state%></td>
<td class="ver_10_black">Время красног ника&nbsp;&nbsp;&nbsp;&nbsp;<%=role.status.invader_time%></td>
<td class="ver_10_black">Время розового ника&nbsp;&nbsp;&nbsp;&nbsp;<%=role.status.pariah_time%></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td colspan="2" class="ver_10_black">Репутация&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="reputation" value="<%=role.status.reputation%>" size="12" maxlength="10"/></td>
<td colspan="2" class="ver_10_black"><input type="checkbox" name="clearstorehousepasswd">Удалить пароль от хранилища&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	</table>
		</td>
		</tr>
<tr><td>&nbsp;</td></tr>
		<tr>
		<td>
	<table width="100%" cellspacing="1" cellpadding="3" bgcolor="#000000">
		<tr bgcolor="#FFFFFF">
<td width="30%" class="ver_10_black">Количество полученого GOLD:</td>
<td class="ver_10_black"><%=role.user.cash_add%></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Количество купленого GOLD:</td>
<td class="ver_10_black"><%=role.user.cash_buy%></td>
		</tr>
		<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Количество проданого GOLD:</td>
<td class="ver_10_black"><%=role.user.cash_sell%></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Всего потрачено GOLD:</td>
<td class="ver_10_black"><%=role.user.cash_used%></td>
		</tr>
	</table>
		</td>
		</tr>
<tr><td>&nbsp;</td></tr>
		<tr>
		<td>
	<table width="100%" cellspacing="1" cellpadding="3" bgcolor="#000000">
		<tr bgcolor="#FFFFFF">
<td width="25%" class="ver_10_black">Уровень:&nbsp;<input type="text" name="level" value="<%=role.status.level%>" size="5" maxlength="3"/></td>
<td width="25%" class="ver_10_black">Культивация:&nbsp;<input type="text" name="level2" value="<%=role.status.level2%>" size="5" maxlength="3"/></td>
<td width="25%" class="ver_10_black">Опыт:&nbsp;<input type="text" name="exp" value="<%=role.status.exp%>" size="12" maxlength="10"/></td>
<td width="25%" class="ver_10_black">Дух:&nbsp;<input type="text" name="sp" value="<%=role.status.sp%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Очки умений:&nbsp;<input type="text" name="pp" value="<%=role.status.pp%>" size="12" maxlength="10"/></td>
<td class="ver_10_black">Координата X:&nbsp;<input type="text" name="posx" value="<%=(int)role.status.posx%>" size="5" maxlength="6"/></td>
<td class="ver_10_black">Координата Y:&nbsp;<input type="text" name="posy" value="<%=(int)role.status.posy%>" size="5" maxlength="6"/></td>
<td class="ver_10_black">Координата Z:&nbsp;<input type="text" name="posz" value="<%=(int)role.status.posz%>" size="5" maxlength="6"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Выносливость:&nbsp;<input type="text" name="vitality" value="<%=role.ep.vitality%>" size="11" maxlength="10"/></td>
<td class="ver_10_black">Интелект:&nbsp;<input type="text" name="energy" value="<%=role.ep.energy%>" size="12" maxlength="10"/></td>
<td class="ver_10_black">Сила:&nbsp;<input type="text" name="strength" value="<%=role.ep.strength%>" size="12" maxlength="10"/></td>
<td class="ver_10_black">Ловкость:&nbsp;<input type="text" name="agility" value="<%=role.ep.agility%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black">Деньги:&nbsp;<input type="text" name="money" value="<%=role.pocket.money%>" size="12" maxlength="10"/></td>
<td class="ver_10_black">Максимум HP:&nbsp;<input type="text" name="max_hp" value="<%=role.ep.max_hp%>" size="12" maxlength="10"/></td>
<td class="ver_10_black">Максимум MP:&nbsp;<input type="text" name="max_mp" value="<%=role.ep.max_mp%>" size="12" maxlength="10"/></td>
<td class="ver_10_black">Рейт атаки:&nbsp;<input type="text" name="attack" value="<%=role.ep.attack%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Минимальная физическая атака:&nbsp;<input type="text" name="damage_low" value="<%=role.ep.damage_low%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальная физическая атака:&nbsp;<input type="text" name="damage_high" value="<%=role.ep.damage_high%>" size="12" maxlength="10"/></td>
		</tr>  
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Минимальная магическая атака:&nbsp;<input type="text" name="damage_magic_low" value="<%=role.ep.damage_magic_low%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальная магическая атака:&nbsp;<input type="text" name="damage_magic_high" value="<%=role.ep.damage_magic_high%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Минимальный урон металом:&nbsp;<input type="text" name="addon_damage_low0" value="<%=role.ep.addon_damage_low[0]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальный урон металом:&nbsp;<input type="text" name="addon_damage_high0" value="<%=role.ep.addon_damage_high[0]%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Минимальный урон деревом:&nbsp;<input type="text" name="addon_damage_low1" value="<%=role.ep.addon_damage_low[1]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальный урон деревом:&nbsp;<input type="text" name="addon_damage_high1" value="<%=role.ep.addon_damage_high[1]%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Минимальный урон водой:&nbsp;<input type="text" name="addon_damage_low2" value="<%=role.ep.addon_damage_low[2]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальный урон водой:&nbsp;<input type="text" name="addon_damage_high2" value="<%=role.ep.addon_damage_high[2]%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Минимальный урон огнем:&nbsp;<input type="text" name="addon_damage_low3" value="<%=role.ep.addon_damage_low[3]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальный урон огнем:&nbsp;<input type="text" name="addon_damage_high3" value="<%=role.ep.addon_damage_high[3]%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Минимальный урон землей:&nbsp;<input type="text" name="addon_damage_low4" value="<%=role.ep.addon_damage_low[4]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальный урон землей:&nbsp;<input type="text" name="addon_damage_high4" value="<%=role.ep.addon_damage_high[4]%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Защита от метала:&nbsp;<input type="text" name="resistance0" value="<%=role.ep.resistance[0]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Защита от дерева:&nbsp;<input type="text" name="resistance1" value="<%=role.ep.resistance[1]%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Защита от воды:&nbsp;<input type="text" name="resistance2" value="<%=role.ep.resistance[2]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Защита от огня:&nbsp;<input type="text" name="resistance3" value="<%=role.ep.resistance[3]%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Защита от земли:&nbsp;<input type="text" name="resistance4" value="<%=role.ep.resistance[4]%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Физическая защита:&nbsp;<input type="text" name="defense" value="<%=role.ep.defense%>" size="12" maxlength="10"/></td>
		</tr>
	<tr bgcolor="#FFFFFF">
<td class="ver_10_black" colspan="2">Уклонение:&nbsp;<input type="text" name="armor" value="<%=role.ep.armor%>" size="12" maxlength="10"/></td>
<td class="ver_10_black" colspan="2">Максимальное количество ярости:&nbsp;<input type="text" name="max_ap" value="<%=role.ep.max_ap%>" size="12" maxlength="10"/></td>
		</tr>
	</table>
		</td>
		</tr>
<tr><td>&nbsp;</td></tr>
<tr><td><span class="ver_14_red">Внимание:  Измение любых полей кроме Опыт, Деньги, Репутация и Дух может привести к падению базы.</span></td></tr>
<tr><td>&nbsp;</td></tr>
		<tr>
	<td align="center">
<input type="submit" class="button" value="Сохранить"/>
<input type="reset" class="button" value="Сбросить"/>
<input type="button" class="button" value="Вернуться назад" onclick="location.href='index.jsp';">
		</td>
		</tr>	
<tr><td>&nbsp;</td></tr>
	</table>
	</form>  
		</td>
		</tr>
	</table>
<CENTER/>
<%@include file="../include/foot.jsp"%>
</body>
</html>

