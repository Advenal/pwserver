'<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>
<%@page import="java.lang.*"%>
<%@page import="java.util.Calendar.*"%>
<%@include file="dungeyadmin.jsp"%>
<% boolean showTag = false;
if(request.getSession().getAttribute("username")!=null) showTag = true;
%>
<html>
<title>Server Control</title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../include/style.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
<%if(showTag)
{%>
<%@include file="/include/header.jsp"%>
</head>
<body>
<div align="center">
<table width="800" border="0" cellspacing="0">
<%
	String message = "<br>";
	boolean allowed = false;

	if(request.getSession().getAttribute("username") == null)


	
	{
		out.println("<p align=\"right\"><font color=\"#ee0000\"><b>Первые вы идете в IWEB!</b></font></p>");
	}
	else
	{
		allowed = true;
	}

	// Apply changes
	if(request.getParameter("process") != null && allowed)
	{
		Process p;
		String command;
		File working_directory;

		if(request.getParameter("process").compareTo("stopserver") == 0)
		{
			try
			{
				command = pw_server_path + "gamedbd/./gamedbd " + pw_server_path + "gamedbd/gamesys.conf exportclsconfig";
				working_directory = new File(pw_server_path + "gamedbd/");
				p = Runtime.getRuntime().exec(command, null, working_directory);
				p.waitFor();
				Thread.sleep(1000);

				Runtime.getRuntime().exec("pkill -9 gs");
				Runtime.getRuntime().exec("pkill -9 glinkd");
				Runtime.getRuntime().exec("pkill -9 mauthd/build/; ./authd");
				Runtime.getRuntime().exec("pkill -9 gdeliveryd");
				Runtime.getRuntime().exec("pkill -9 gfactiond");
				Runtime.getRuntime().exec("pkill -9 gacd");
				Runtime.getRuntime().exec("pkill -9 gamedbd");
				Runtime.getRuntime().exec("pkill -9 uniquenamed");
				p = Runtime.getRuntime().exec("ps -A w");
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line;
				while((line = input.readLine()) != null)
				{
					if(line.indexOf("authd") != -1)
					{
						Runtime.getRuntime().exec("kill " + line.substring(0, 5).replace(" ", ""));
					}
				}
				Runtime.getRuntime().exec("pkill -9 logservice");
				Runtime.getRuntime().exec("sync");
				FileWriter fw = new FileWriter(new File("/proc/sys/vm/drop_caches"));
				fw.write("3");
				fw.close();

				Thread.sleep(6000);

				message = "<font color=\"#00cc00\"><b>Сервер выключен!</font>";
			}
			catch(Exception e)
			{
				message = "<font color=\"#ee0000\"><b>Выключить сервер не удалось!</b></font>";
			}
		}

		if(request.getParameter("process").compareTo("startserver") == 0)
		{
			File f = new File(pw_server_path + "iweb_starter.sh");
			try
			{
				FileWriter fw = new FileWriter(f);
				fw.write("if [ ! -d " + pw_server_path + "logs ]; then\n");
				fw.write("mkdir " + pw_server_path + "logs\n");
				fw.write("fi\n");
				fw.write("cd " + pw_server_path + "logservice; ./logservice logservice.conf >" + pw_server_path + "logs/logservice.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "uniquenamed; ./uniquenamed gamesys.conf >" + pw_server_path + "logs/uniquenamed.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "mauthd/build/; ./authd>" + pw_server_path + "logs/auth.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gamedbd; ./gamedbd gamesys.conf >" + pw_server_path + "logs/gamedbd.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gacd; ./gacd gamesys.conf >" + pw_server_path + "logs/gacd.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gfactiond; ./gfactiond gamesys.conf >" + pw_server_path + "logs/gfactiond.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gdeliveryd; ./gdeliveryd gamesys.conf >" + pw_server_path + "logs/gdeliveryd.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 1 >" + pw_server_path + "logs/glink1.log &\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 2 >" + pw_server_path + "logs/glink2.log &\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 3 >" + pw_server_path + "logs/glink3.log &\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 4 >" + pw_server_path + "logs/glink4.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gamed; ./gs gs01 >" + pw_server_path + "logs/gs01.log &\n");
				fw.write("sleep 1");
				fw.close();

				command = "sh " + pw_server_path + "iweb_starter.sh";
				working_directory = new File(pw_server_path);
				Runtime.getRuntime().exec("chmod 777 " + pw_server_path + "iweb_starter.sh");
				Runtime.getRuntime().exec(command, null, working_directory);
				Thread.sleep(12000);
				f.delete();

				message = "<font color=\"#00cc00\"><b>Сервер работает под управлением ... Это может занять некоторое время, чтобы полностью загрузиться.</font>";
			}
			catch(Exception e)
			{
				f.delete();
				message = "<font color=\"#ee0000\"><b>Сервер не удалось запустить!</b></font>" + e.getMessage();
			}
		}

		if(request.getParameter("process").compareTo("stopallmaps") == 0)
		{
			try
			{
				int time = Integer.parseInt(request.getParameter("time"));
				if(protocol.DeliveryDB.GMRestartServer(-1, time))
				{
					message = "<font color=\"#00cc00\"><b>Карты отключены за " + time + " секунд!</font>";
				}
				else
				{
					message = "<font color=\"#ee0000\"><b>Выключить карты не удалась!</b></font>";
				}
			}
			catch(Exception e)
			{
				message = "<font color=\"#ee0000\"><b>Параметры в режиме реального времени.</b></font>";
			}
		}

		if(request.getParameter("process").compareTo("stopmap") == 0)
		{
			try
			{
				String[] maps = request.getParameterValues("map");
				for(int i=0; i<maps.length; i++)
				{
					Runtime.getRuntime().exec("kill " +  maps[i]);
					Thread.sleep(1000);
				}
				message = "<font color=\"#00cc00\"><b>Карта остановлена</b></font>";
			}
			catch(Exception e)
			{
				message = "<font color=\"#ee0000\"><b>Не удалось остановить карты!</b></font>";
			}
		}

		if(request.getParameter("process").compareTo("startmap") == 0)
		{
			File f = new File(pw_server_path + "iweb_map.sh");
			try
			{
				String[] maps = request.getParameterValues("map");
				FileWriter fw = new FileWriter(f);
				for(int i=0; i<maps.length; i++)
				{
					fw.write("cd " + pw_server_path + "gamed; ./gs " + maps[i] + " >" + pw_server_path + "logs/" + maps[i] + ".log &\n");
					fw.write("sleep 1");
				}
				fw.close();

				command = "sh " + pw_server_path + "./iweb_map.sh";
				working_directory = new File(pw_server_path);
				Runtime.getRuntime().exec("chmod 755 " + pw_server_path + "iweb_map.sh");
				Runtime.getRuntime().exec(command, null, working_directory);
				Thread.sleep(1000*maps.length);
				f.delete();
				message = "<font color=\"#00cc00\"><b>Карта запущена</b></font>";
			}
			catch(Exception e)
			{
				f.delete();
				message = "<font color=\"#ee0000\"><b>Карты не запущены !!!</b></font>";
			}
		}
		if(request.getParameter("process").compareTo("backup") == 0)
		{
			// Check if another Backup is running

			String line;
			boolean backup_allowed = true;
			p = Runtime.getRuntime().exec("ps -A w");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while((line = input.readLine()) != null)
			{
				if(line.substring(27).indexOf("/./pw_backup.sh") != -1)
				{
					backup_allowed = false;
				}
			}
			input.close();

			if(backup_allowed)
			{
				File f = new File(pw_server_path + "pw_backup.sh");
				try
				{
					String time = (new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss")).format(new java.util.Date());
					FileWriter fw = new FileWriter(f);
					fw.write("cd " + pw_server_path + "\n");
					fw.write("mysqldump -u" + db_user + " -p" + db_password + " " + db_database + " --routines > " + pw_server_path + "pw_backup_" + time + ".sql\n");
					fw.write("sleep 1\n");
					fw.write("tar -zcf " + pw_server_path + "pw_backup_" + time + ".tar.gz " + pw_server_path + " --exclude=pw_backup*\n");
					fw.write("sleep 1\n");
					fw.write("sync; echo 3 > /proc/sys/vm/drop_caches");
					fw.close();

					command = "sh " + pw_server_path + "./pw_backup.sh";
					working_directory = new File(pw_server_path);
					Runtime.getRuntime().exec("chmod 755 " + pw_server_path + "pw_backup.sh");
					Runtime.getRuntime().exec(command, null, working_directory);

					Thread.sleep(3000);

					f.delete();
					message = "<font color=\"#00cc00\"><b>Backup iniciado -> " + pw_server_path + "pw_backup_" + time + "</b></font>";
				}
				catch(Exception e)
				{
					f.delete();
					message = "<font color=\"#ee0000\"><b>Backup falhou</b></font>";
				}
			}
		}
	}


	// Load

	// maps[*][0] -> process ID / pid
	// maps[*][1] -> map id
	// maps[*][2] -> map name
	String[][] maps = 
	{
		{"0", "gs01", "World"}, 
		{"0", "is01", "City of Abominations"}, 
		{"0", "is02", "Secret Passage"}, 
		{"0", "is03", "Unknown"}, 
		{"0", "is04", "Unknown"}, 
		{"0", "is05", "Firecrag Grotto"}, 
		{"0", "is06", "Den of Rabid Wolves"}, 
		{"0", "is07", "Cave of the Vicious"}, 
		{"0", "is08", "Hall of Deception"}, 
		{"0", "is09", "Gate of Delerium"}, 
		{"0", "is10", "Secret Frostcover Grounds"}, 
		{"0", "is11", "Valley of Disaster"}, 
		{"0", "is12", "Forest Ruins"}, 
		{"0", "is13", "Cave of Sadistic Glee"}, 
		{"0", "is14", "Wraithgate"}, 
		{"0", "is15", "Hallucinatory Trench"}, 
		{"0", "is16", "Eden"}, 
		{"0", "is17", "Brimstone Pit"}, 
		{"0", "is18", "Temple of the Dragon"}, 
		{"0", "is19", "Nightscream Island"}, 
		{"0", "is20", "Snake Isle"}, 
		{"0", "is21", "Lothranis"}, 
		{"0", "is22", "Momaganon"}, 
		{"0", "is23", "Seat of Torment"}, 
		{"0", "is24", "Abaddon"}, 
		{"0", "is25", "City of Naught"}, 
		{"0", "is26", "Hall of Blasphemy"}, 
		{"0", "is27", "Lunar Glade"}, 
		{"0", "is28", "Valley of Reciprocity"}, 
		{"0", "is29", "Frostcover City"}, 
		{"0", "is30", "The Palace of Dark Sand"}, 
		{"0", "is31", "Twighlight Temple"}, 
		{"0", "is32", "Cube of Fate"}, 
		{"0", "is33", "Chrono City"}, 
		{"0", "is34", "Perfect Chapel"}, 
		{"0", "is35", "Faction House"},
		{"0", "is37", "Morai"},
		{"0", "is38", "Phoenix Valley"},
		{"0", "is39", "Sem nome"},
		{"0", "is40", "Sem nome"},
		{"0", "arena01", "Arena - GM"}, 
		{"0", "arena02", "Arena - GO"}, 
		{"0", "arena03", "Arena - GP"}, 
		{"0", "arena04", "Arena - DG"}, 		
		{"0", "bg01", "Battle Ground - 1"}, 
		{"0", "bg02", "Battle Ground - 2"}, 
		{"0", "bg03", "Battle Ground - 3"}, 
		{"0", "bg04", "Battle Ground - 4"}, 
		{"0", "bg05", "Battle Ground - 5"}, 
		{"0", "bg06", "Battle Ground - 6"}
	};

	boolean server_running = false;
	boolean backup_running = false;
	int log_count = 0;
	int auth_count = 0;
	int unique_count = 0;
	int gac_count = 0;
	int gfaction_count = 0;
	int gdelivery_count = 0;
	int glink_count = 0;
	int gamedb_count = 0;
	int map_count = 0;
	String[] mem;
	String[] swp;
	String line;
	String process;
	Process p;
	BufferedReader input;

	p = Runtime.getRuntime().exec("free -m");
	input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	input.readLine();
	mem = input.readLine().split("\\s+");
	input.readLine();
	swp = input.readLine().split("\\s+");
	input.close();

	p = Runtime.getRuntime().exec("ps -A w");
	input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	while((line = input.readLine()) != null)
	{
		process = line.substring(27);

		if(process.indexOf("./pw_backup.sh") != -1)
		{
			message += "<br><font color=\"#ee0000\"><b>Запуск, подождите, пока резервное копирование не завершено! <br>Pressione <a href=\"dungey.jsp?page=dungey\"><font color=\"#0000cc\">aqui</font></a> , ожидайте, пока не исчезнет сообщение ...</b></font>";
		}

		if(process.indexOf("./logservice") != -1)
		{
			log_count++;
			server_running = true;
		}
		if(process.indexOf("./authd") != -1)
		{
			auth_count++;
			server_running = true;
		}
		if(process.indexOf("./uniquenamed") != -1)
		{
			unique_count++;
			server_running = true;
		}
		if(process.indexOf("./gacd") != -1)
		{
			gac_count++;
			server_running = true;
		}
		if(process.indexOf("./gfactiond") != -1)
		{
			gfaction_count++;
			server_running = true;
		}
		if(process.indexOf("./gdeliveryd") != -1)
		{
			gdelivery_count++;
			server_running = true;
		}
		if(process.indexOf("./glinkd") != -1)
		{
			glink_count++;
		}
		if(process.indexOf("./gamedb") != -1)
		{
			gamedb_count++;
			server_running = true;
		}

		// Check running maps
		for(int i=0; i<maps.length; i++)
		{
			if (process.indexOf("./gs " + maps[i][1]) != -1)
			{
				// set the process id
				maps[i][0] = line.substring(0, 5).replace(" ", "");
				map_count++;
				server_running = true;
			}
		}
	}
	input.close();
%>

<table width="800" cellpadding="0" cellspacing="0" border="0">

<tr>
	<td align="center" colspan="2">
		<%= message %>
	</td>
</tr>

<tr>
	<td colspan="2">
		<br>
	</td>
</tr>

<tr>
<td align="center" valign="top">

	<table width="380" cellpadding="2" cellspacing="0" style="border: 1px solid #000000;">
		<tr>
			<th colspan="4" bgcolor="#000000">
				<b><font color="#ffffff">Serviços</font></b>
			</th>
		</tr>
		<tr bgcolor="#f0f0f0">
			<td>
				<b>Memoria (MB)</b>
			</td>
			<td align="center">
				<b>Total<b>
			</td>
			<td align="center">
				<b>Usada<b>
			</td>
			<td align="center">
				<b>Livre<b>
			</td>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				RAM:
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
			<%
				out.println(mem[1]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
			<%
				out.println(mem[2]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
			<%
				out.println(mem[3]);
			%>
			</td>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				SWAP:
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
			<%
				out.println(swp[1]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
			<%
				out.println(swp[2]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
			<%
				out.println(swp[3]);
			%>
			</td>
		</tr>
		<tr bgcolor="#f0f0f0">
			<td style="border-top: 1px solid #000000;">
				<b>Descrição</b>
			</td>
			<td style="border-top: 1px solid #000000;">
				<b>Processos</b>
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
				<b>#</b>
			</td>
			<td align="center" style="border-top: 1px solid #000000;">
				<b>Status</b>
			</td>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Logservice
			</td>
			<td style="border-top: 1px solid #000000;">
				./logservice
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + log_count + "</td>");
				if(log_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Auth Daemon
			</td>
			<td style="border-top: 1px solid #000000;">
				./authd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + auth_count + "</td>");
				if(auth_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Unique Name Daemon
			</td>
			<td style="border-top: 1px solid #000000;">
				./uniquenamed
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + unique_count + "</td>");
				if(unique_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Game Anti-Cheat Daemon
			</td>
			<td style="border-top: 1px solid #000000;">
				./gacd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + gac_count + "</td>");
				if(gac_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Faction Daemon
			</td>
			<td style="border-top: 1px solid #000000;">
				./gfactiond
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + gfaction_count + "</td>");
				if(gfaction_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Game Delivery Daemon
			</td>
			<td style="border-top: 1px solid #000000;">
				./gdeliveryd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + gdelivery_count + "</td>");
				if(gdelivery_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Game Link Daemon
			</td>
			<td style="border-top: 1px solid #000000;">
				./glinkd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + glink_count + "</td>");
				if(glink_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Game Database Daemon
			</td>
			<td style="border-top: 1px solid #000000;">
				./gamedbd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + gamedb_count + "</td>");
				if(gamedb_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td style="border-top: 1px solid #000000;">
				Map Service
			</td>
			<td style="border-top: 1px solid #000000;">
				./gs
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\">" + map_count + "</td>");
				if(map_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #000000;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr>
			<td colspan=4" align="center" style="border-top: 1px solid #000000;">
			<form action="dungey.jsp?page=dungey" method="post" style="margin: 0px;">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
			<%
				if(allowed)
				{
					if(server_running)
					{
						out.print("<td style=\"padding-right: 2px;\"><input type=\"hidden\" name=\"process\" value=\"stopserver\"></input>");
						out.print("<input type=\"image\" src=\"images/btn_stop.jpg\"  style=\"border: 0px;\"></input>");
					}
					else
					{
						out.print("<td><input type=\"hidden\" name=\"process\" value=\"startserver\"></input>");
						out.print("<input type=\"image\" src=\"images/btn_start.jpg\"  style=\"border: 0px;\"></input></td>");
					}
					out.print("<td style=\"padding-left: 2px;\"><a href=\"dungey.jsp?page=dungey&process=backup\"><img src=\"images/btn_backup.jpg\" border=\"0\"></img></a></td>");
				}
			%>
			</tr>
			</table>
			</form>
			</td>
		</tr>
	</table>
</td>

<td align="center" valign="top">

	<table width="380" cellpadding="2" cellspacing="0"  style="border: 1px solid #000000;">
		<tr>
			<th colspan="4" bgcolor="#000000">
				<b><font color="#ffffff">Mapas</font></b>
			</th>
		</tr>
		<tr bgcolor="#f0f0f0">
			<td align="center" colspan="2" style="border-top: 1px solid #000000;">
				<b>Tempo do servidor (desligar todos os mapas)<b>
			</td>
		</tr>
		<tr>
			<td align="left">
				Desligar (segundos):
			</td>
			<td align="right">
				<form action="dungey.jsp?page=dungey&process=stopallmaps" method="post" style="margin: 0px;">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr>
				<td><input type="text" name="time" value="300" style="width: 50px; text-align: center;"></input></td>
				<td style="padding-left: 2px;"><input type="image" src="images/btn_stop.jpg"  style="border: 0px;"></input></td>
				</tr>
				</table>
				</form>
			</td>
		</tr>
		<tr bgcolor="#f0f0f0">
			<td width="50%" align="center">
				<b>Mapas Online</b>
			</td>
			<td width="50%" align="center">
				<b>Mapas Disponíveis</b>
			</td>
		</tr>
		<tr>
			<td align="center">
				<form action="dungey.jsp?page=dungey&process=stopmap" method="post" solstyle="margin: 0px;">
				<select name="map" size="11" multiple="multiple" style="width: 100%;">
				<%
					for(int i=0; i<maps.length; i++)
					{
						if(maps[i][0].compareTo("0") != 0)
						{
							out.println("<option value=\"" + maps[i][0] + "\">" + maps[i][1] + " : " + maps[i][2] + "</option>");
						}
					}
				%>
				</select>
				<%
					if(allowed)
					{
						out.print("<input type=\"image\" title=\"Parar os mapas selecionados\" src=\"images/btn_stop.jpg\" style=\"border: 0px; padding: 2px;\"></input>");
					}
				%>
				</form>
			</td>
			<td align="center">
				<form action="dungey.jsp?page=dungey&process=startmap" method="post" style="margin: 0px;">
				<select name="map" size="11" multiple="multiple" style="width: 100%;">
				<%
					for(int i=0; i<maps.length; i++)
					{
						if(maps[i][0].compareTo("0") == 0)
						{
							out.println("<option value=\"" + maps[i][1] + "\">" + maps[i][1] + " : " + maps[i][2] + "</option>");
						}
					}
				%>
				</select>
				<%
					if(allowed)
					{
						out.print("<input type=\"image\" title=\"Iniciar os mapas selecionados\" src=\"images/btn_start.jpg\" style=\"border: 0px; padding: 2px;\"></input>");
					}
				%>
				</form>
			</td>
		</tr>
	</table>
</td>
</tr>
</table>
</div>
<%
} else
{%>
<%@include file="/include/nologin.jsp"%><%
} %>
<br>
<%@include file="/include/foot.jsp"%>
</body>
</html>