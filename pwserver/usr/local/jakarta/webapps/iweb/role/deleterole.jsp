<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.goldhuman.auth.AuthFilter"%>
<%@ page import="com.goldhuman.service.interfaces.LogInfo"%>
<%@ page import="com.goldhuman.service.interfaces.GMService"%>
<%@ page import="com.goldhuman.service.GMServiceImpl"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>

<%
			String userid = "";
			String roleid = "";
			String rolename = "";
			String biao = request.getParameter("biao");
			if (biao != null && biao.equals("1")) {
				userid = request.getParameter("userid");
			}
			if (biao != null && biao.equals("2")) {
				roleid = request.getParameter("roleid");
				if (roleid != null && roleid.trim().length() > 0) {
					try {
						int rid = Integer.parseInt(roleid);
						GMService gs = new GMServiceImpl();
						int tmpid = gs.roleid2Uid(rid, new LogInfo());
						if (-1 == tmpid)
							throw new Exception("roleid2Uid error");
						userid = Integer.toString(tmpid);
					} catch (Exception e) {
						out.println("Please input number!&nbsp;<font color=red size=2>"
								+ e.getMessage() + "</font>");

					}
				}
			}
			if (biao != null && biao.equals("3")) {
				rolename = request.getParameter("rolename");
				if (rolename != null && rolename.trim().length() > 0) {
					try {
						GMService gs = new GMServiceImpl();
						int tem = gs.getRoleIdByName(rolename, new LogInfo());
						if (-1 == tem)
							throw new Exception("getRoleIdByName error");
						roleid = Integer.toString(tem);
						int tmpid = gs.roleid2Uid(new Integer(tem), new LogInfo());
						if (-1 == tmpid)
							throw new Exception("roleid2Uid error");
						userid = Integer.toString(tmpid);
					} catch (Exception e) {
						out.println("<font color=red size=2>"+ LocaleUtil.getMessage(request,"role_deleterole_nofound") + "</font>");
					}
				}
			}

			LogFactory.getLog("deleterole.jsp").info(
					"useid=" + userid + ",rolename=" + rolename + ","
							+ "operator=" + AuthFilter.getRemoteUser(session));

			%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>Удаление персонажа</title>
		<link href="../include/style.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<%@include file="../include/header.jsp"%>

		<table width="100%" height="514" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">

			<tr>
				<td>
					<TABLE align="center" border="1" cellpadding="0" cellspacing="1" width="500px">
						<TR>
							<TH>
								Аккаунт
							</TH>
							<TH>
								Персонаж	
							</TH>	
							<TH>
								Никнейм
							</TH>
							<TH>
								Статус
							</TH>
						</TR>
						<%LogInfo info = null;
			int uid = -1;
			GMService gs = new GMServiceImpl();
			if (userid != null && userid.trim().length() > 0) {
				int flag = -1;
				if (biao != null && biao.equals("3")) {
					try {
						uid = Integer.parseInt(userid);
					} catch (Exception e) {
						out .println("\"" + LocaleUtil.getMessage(request,"role_deleterole_inputroleid") + "\"" 
							+ LocaleUtil.getMessage(request,"role_deleterole_inputnum") 
							+ " &nbsp;<font color=red size=2>" + e.getMessage() + "</font>");
					}
					info = new LogInfo(uid, "", LocaleUtil.getMessage(request,"role_deleterole_delerole"));

					flag = gs.deleteRole(uid, rolename, info);
				} else if (biao != null && biao.equals("2")) {
					try {
						uid = Integer.parseInt(userid);
					} catch (Exception e) {
						out .println("\"" + LocaleUtil.getMessage(request,"role_deleterole_inputroleid") + "\"" 
							+ LocaleUtil.getMessage(request,"role_deleterole_inputnum") 
							+ " &nbsp;<font color=red size=2>" + e.getMessage() + "</font>");
					}
					info = new LogInfo(uid, "", LocaleUtil.getMessage(request,"role_deleterole_delerole"));
					flag = ((GMServiceImpl)gs).deleteRole(Integer.parseInt(roleid), info);
				}
				String result = null;
				switch (flag) {
				case 0:
					result = LocaleUtil.getMessage(request,"role_deleterole_succ");
					break;

				case 3:
					result = LocaleUtil.getMessage(request,"role_deleterole_rolenotexist");
					break;

				case 4:
					result = LocaleUtil.getMessage(request,"role_deleterole_rolenotmatchacco");
					break;

				case -1:
					result = LocaleUtil.getMessage(request,"role_deleterole_timeout");
				}

				%>
						<TR>
							<TD>
								<%=uid%>
							</TD>
							<TD>
								<%=roleid%>
							</TD>
							<TD>
								<%=rolename%>
							</TD>
							<TD>
								<%=result%>
							</TD>
						</TR>
						<%}%>
					</TABLE>
				</td>
			</tr>

			<tr>
				<td align="center">
					<form name="form1" action="deleterole.jsp" method="post">
						Удаление персонажа
						<table border="0">
							<tr>
								<td>
									<input type="radio" name="biao" value="2" onclick="show(2)">
									Введите ID персонажа:
								</td>
								<td>
									<div id="rid" style="display:none">
										<input type="text" name="roleid" value="<%=roleid%>" size="16" maxlength="10" />
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="biao" value="3" onclick="show(3)">
									Введите ник персонажа:
								</td>
								<td>
									<!-- <%= LocaleUtil.getMessage(request,"role_deleterole_inputrolename") %> -->
									<div id="rname" style="display:none">
										<input type="text" name="rolename" value="<%=rolename%>" size="16" maxlength="15" />
									</div>
								</td>
							</tr>
						</table>

						<input type="submit" value="УДАЛИТЬ!">
					</form>
				</td>
			</tr>

		</table>
		<%@include file="../include/foot.jsp"%>

		<script language="javascript">
<!--

    function show(t){
      if(t==2)  
        document.getElementById("rid").style.display="";
      else
        document.getElementById("rid").style.display="none"; 
      if(t==3)
        document.getElementById("rname").style.display="";
      else 
        document.getElementById("rname").style.display="none";       
          
    
    }

-->

</script>

	</body>
</html>
