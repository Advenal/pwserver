﻿<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.goldhuman.util.LocaleUtil" %>

<html>
	<head>
		<link href="../include/style.css" rel="stylesheet" type="text/css">
		<title>Управление персонажами</title>
	</head>
	<body bgcolor="white">
		<%@include file="../include/header.jsp"%>

		<table width="100%" height="513" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
			<tr>
				<td>
					<table border="1" cellpadding="5" align="center" width=600>
						<TR>
							<TH align=left width=40%>
								Операция
							</TH>
							<TH align=left>
								Описание
							</Th>
						</TR>
						
						<TR>
							<TD colspan="2" style="background-color:black;">
							</TD>
						</TR>
						
						<TR>
							<TD>
								<a href="createrole.jsp">Создать перса</a>
							</TD>
							<TD>
								Создание нового персонажа на данном аккаунте
							</TD>
						</TR>
						<TR>
							<TD>
								<a href="deleterole.jsp">Удалить персонажа</a>
							</TD>
							<TD>
								Удаляет указанного персонажа из указанного аккаунта
							</TD>
						</TR>

						<TR>
							<TD colspan="2" style="background-color:black;">
							</TD>
						</TR>
						
						<TR>
							<TD>
								<a href="userrolecount.jsp">Лимит аккаунта</a>
							</TD>
							<TD>
								Показывает количество персонажей на аккаунте
							</TD>
						</TR>
						<TR>
							<TD>
								<a href="rolelist.jsp">Список персов</a>
							</TD>
							<TD>
								Показывает список персонажей на аккаунте
							</TD>
						</TR>
						<TR>
							<TD>
								<a href="rolelogstatus.jsp">Онлайн-статус</a>
							</TD>
							<TD>
								Показывает онлайн статус персонажа
							</TD>
						</TR>
						
						<TR>
							<TD colspan="2" style="background-color:black;">
							</TD>
						</TR>
						
						<TR>
							<TD>
								<a href="cashinfo.jsp">Информация о золоте</a>
							</TD>
							<TD>
								Информация о денежных средствах игрока (перс\акк)
							</TD>
						</TR>
						<TR>
							<TD>
								<a href="modifyroledata.jsp">Редактировать перса</a>
							</TD>
							<TD>
								[не работает] Меняет базовую информацию персонажа
							</TD>
						</TR>
						
						<TR>
							<TD colspan="2" style="background-color:black;">
							</TD>
						</TR>
						
						<TR>
							<TD>
								<a href="canchangerolename.jsp">Проверка смены имени</a>
							</TD>
							<TD>
								Проверяет, возможно ли поменять данное имя
							</TD>
						</TR>
						<TR>
							<TD>
								<a href="renamerole.jsp">Смена имени</a>
							</TD>
							<TD>
								Меняет имя персонажа по указанному ID персонажа или аккаунта
							</TD>
						</TR>
						<TR>
							<TD>
								<a href="rolenameexists.jsp">Поиск имени</a>
							</TD>
							<TD>
								Проверяет, существует ли данное имя персонажа на сервере
							</TD>
						</TR>
						
						<TR>
							<TD colspan="2" style="background-color:black;">
							</TD>
						</TR>
						
						<TR>
							<TD>
								<a href="forbidrole.jsp">Блокировки</a>
							</TD>
							<TD>
								Средства блокировки персонажей и аккаунтов
							</TD>
						</TR>

					</TABLE>
				</td>
			</tr>
		</table>
		<%@include file="../include/foot.jsp"%>
	</body>
</html>
