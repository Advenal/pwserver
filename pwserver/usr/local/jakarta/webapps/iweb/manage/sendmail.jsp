<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>
<%@page import="protocol.*"%>
<%@page import="com.goldhuman.Common.Octets"%>
<html>
    <head>
        <link href="../include/style.css" rel="stylesheet" type="text/css">
        <title><%= LocaleUtil.getMessage(request, "include_header_sendmill")%></title>
    </head>
    <%!
        byte[] hextoByteArray(String x) {
            if (x.length() < 2) {
                return new byte[0];
            }
            if (x.length() % 2 != 0) {
                System.err.println("hextoByteArray error! hex size=" + Integer.toString(x.length()));
            }
            byte[] rb = new byte[x.length() / 2];
            for (int i = 0; i < rb.length; ++i) {
                rb[i] = 0;

                int n = x.charAt(i + i);
                if ((n >= 48) && (n <= 57)) {
                    n -= 48;
                } else if ((n >= 97) && (n <= 102)) {
                    n = n - 97 + 10;
                }
                rb[i] = (byte) (rb[i] | n << 4 & 0xF0);

                n = x.charAt(i + i + 1);
                if ((n >= 48) && (n <= 57)) {
                    n -= 48;
                } else if ((n >= 97) && (n <= 102)) {
                    n = n - 97 + 10;
                }
                rb[i] = (byte) (rb[i] | n & 0xF);
            }
            return rb;
        }
    %>
    <%
        String message = "<br>";

        if (request.getParameter("process") != null && request.getParameter("process").compareTo("mail") == 0) {
            if (request.getParameter("roleid") != "" && request.getParameter("title") != ""
                    && request.getParameter("content") != "" && request.getParameter("coins") != "") {
                int roleid = Integer.parseInt(request.getParameter("roleid"));
                String title = request.getParameter("title");
                String content = request.getParameter("content");
                int coins = Integer.parseInt(request.getParameter("coins"));

                String itemids = request.getParameter("itemid");
                int itemnum = Integer.parseInt(request.getParameter("itemnum"));
                itemnum = itemnum > 0 ? itemnum : 1;
                String itemhex = request.getParameter("itemhex");
                if (itemhex.length() > 1) {
                    itemnum = 1;
                }
                String[] itemattr = itemids.split(",");
                for (int i = 0; i < itemattr.length; i++) {
                    int itemid = 0;
                    try {
                        itemid = Integer.valueOf(itemattr[i]);
                    } catch (NumberFormatException e) {
                    }
                    GRoleInventory gri = new GRoleInventory();

                    if (itemid > 0 && itemhex.length() > 0) {
                        gri.id = itemid;
                        gri.guid1 = 0;
                        gri.guid2 = 0;
                        gri.mask = 1;
                        gri.proctype = 0;
                        gri.pos = 0;
                        gri.count = itemnum;
                        gri.max_count = 1;
                        gri.expire_date = 0;
                        gri.data = new Octets(hextoByteArray(itemhex));
                    }

                    if (protocol.DeliveryDB.SysSendMail(roleid, title, content, gri, coins)) {
                        message = "<font color=\"#00cc00\"><b>" + LocaleUtil.getMessage(request, "manage_sendmail_success") + "！</b></font>";
                    } else {
                        message = "<font color=\"#ee0000\"><b>" + LocaleUtil.getMessage(request, "manage_sendmail_false") + "！</b></font>";
                    }
                }
            } else {
                message = "<font color=\"#ee0000\"><b>" + LocaleUtil.getMessage(request, "manage_sendmail_alert_inputnull") + "！</b></font>";
            }
        }
    %>

    <body>
        <%@include file="../include/header.jsp"%>
        <table width="100%" height="350"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
            <tr>
                <td align="center"><form action="?process=mail" method="post">
                        <table align="center" width="600" cellpadding="2" cellspacing="0" style="border: 1px solid #cccccc;">
                            <tr>
                                <th height="1" colspan="2" style="padding: 5;"> <b><%=LocaleUtil.getMessage(request, "manage_sendmail_title")%></b> </th>
                            </tr>
                            <tr bgcolor="#f0f0f0">
                                <td colspan="2" align="center" height="1"><% out.print(message);%></td>
                            </tr>
                            <tr>
                                <td height="1" width="120"><%=LocaleUtil.getMessage(request, "manage_sendmail_mroleid")%>: </td>
                                <td height="1" width="480"><input type="text" name="roleid" style="width: 100%; text-align: left;"/>
                                    <b style="color:red">*</b></td>
                            </tr>
                            <tr>
                                <td height="1"><%=LocaleUtil.getMessage(request, "manage_sendmail_mtitle")%>: </td>
                                <td height="1"><input type="text" value="SystemMail" name="title" style="width: 100%; text-align: left;"/>
                                    <b style="color:red">*</b></td>
                            </tr>
                            <tr>
                                <td height="1" valign="top"><%=LocaleUtil.getMessage(request, "manage_sendmail_mcontent")%>: </td>
                                <td height="1"><textarea name="content" rows="5" style="width: 100%; text-align: left;">0</textarea>
                                    <b style="color:red">*</b></td>
                            </tr>
                            <tr>
                                <td height="1" valign="top"><%=LocaleUtil.getMessage(request, "manage_sendmail_mcoins")%>: </td>
                                <td height="1"><input type="text" name="coins" value="0" style="width: 100%; text-align: left;">
                                    </input></td>
                            </tr>
                            <tr>
                                <td height="1" valign="top"><%=LocaleUtil.getMessage(request, "manage_sendmail_mitemid")%>: </td>
                                <td height="1"><input type="text" name="itemid" style="width: 100%; text-align: left;">
                                    <p style="color:#666"><%=LocaleUtil.getMessage(request, "manage_sendmail_mitemidc")%></p></td>
                            </tr>
                            <tr>
                                <td height="1" valign="top"><%=LocaleUtil.getMessage(request, "manage_sendmail_mitemnum")%>: </td>
                                <td height="1"><input type="text" name="itemnum" style="width: 100%; text-align: left;" value="1">
                                    <p style="color:#666"><%=LocaleUtil.getMessage(request, "manage_sendmail_mitemnumc")%></p></td>
                            </tr>
                            <tr>
                                <td height="1" valign="top"><%=LocaleUtil.getMessage(request, "manage_sendmail_mitemhex")%>: </td>
                                <td height="1"><textarea name="itemhex" rows="5" style="width: 100%; text-align: left;">0</textarea>
                                    <p style="color:#666"><%=LocaleUtil.getMessage(request, "manage_sendmail_mitemhexc")%></p></td>
                            </tr>
                            <tr bgcolor="#f0f0f0">
                                <td colspan="2" align="center" height="1">
                                    <input type="submit" name="submit" value="<%= LocaleUtil.getMessage(request, "submit")%>" /></td>
                            </tr>
                        </table>
                    </form></td>
            </tr>
        </table>
        <%@include file="../include/foot.jsp"%>
    </body>
</html>