<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.*"%>
<%@page import="protocol.*"%>
<%@page import="com.goldhuman.auth.*"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>


<%
String strMax = new String("-1");
String strFakeMax = new String("-1");
String strCur = new String("-1");
String strNewMax = new String("5000");
String strNewFakeMax = new String("5000");
{
	Integer[] curmax = new Integer[3];
	if( !DeliveryDB.GetMaxOnlineNum( curmax ) )
		LogFactory.getLog("index.jsp").info("GetMaxOnlineNum error." );
	if( null == curmax[0] )	curmax[0] = new Integer(-1);
	if( null == curmax[1] )	curmax[1] = new Integer(-1);
	if( null == curmax[2] )	curmax[2] = new Integer(-1);
	if( -1 != curmax[0].intValue() ) strNewMax=(new Integer(curmax[0].intValue())).toString();
	if( -1 != curmax[1].intValue() ) strNewFakeMax=(new Integer(curmax[1].intValue())).toString();
	strMax = curmax[0].toString();
	strFakeMax = curmax[1].toString();
	strCur = curmax[2].toString();
}

String strDoubleExp = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	Double v = new com.goldhuman.service.GMServiceImpl().getw2iexperience(new com.goldhuman.service.interfaces.LogInfo());
	if (v != null) {
		strDoubleExp = v.toString();
	}
	/*
	if (strDoubleExp.equals("1") || strDoubleExp.equals("1.0")) {
		strDoubleExp =  "1==1?¡À?";
	}
	*/
	/*
	byte status = DeliveryDB.GMGetDoubleExp();
	if( (byte)1 == status )			strDoubleExp = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strDoubleExp = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strDoubleExp_check = "";
	*/
}

String strLambda = "-1";
{
	int lambda = DeliveryDB.GMGetLambda();
	strLambda = (new Integer(lambda)).toString();
}

String strNoTrade_check = "checked=\"checked\"";
String strNoTrade = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetNoTrade();
	if( (byte)1 == status )			strNoTrade = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strNoTrade = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strNoTrade_check = "";
}

String strNoAuction_check = "checked=\"checked\"";
String strNoAuction = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetNoAuction();
	if( (byte)1 == status )			strNoAuction = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strNoAuction = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strNoAuction_check = "";
}

String strNoMail_check = "checked=\"checked\"";
String strNoMail = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetNoMail();
	if( (byte)1 == status )			strNoMail = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strNoMail = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strNoMail_check = "";
}

String strNoFaction_check = "checked=\"checked\"";
String strNoFaction = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetNoFaction();
	if( (byte)1 == status )			strNoFaction = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strNoFaction = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strNoFaction_check = "";
}

String strDoubleMoney_check = "checked=\"checked\"";
String strDoubleMoney = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetDoubleMoney();
	if( (byte)1 == status )			strDoubleMoney = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strDoubleMoney = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strDoubleMoney_check = "";
}

String strDoubleObject_check = "checked=\"checked\"";
String strDoubleObject = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetDoubleObject();
	if( (byte)1 == status )			strDoubleObject = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strDoubleObject = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strDoubleObject_check = "";
}

String strDoubleSP_check = "checked=\"checked\"";
String strDoubleSP = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetDoubleSP();
	if( (byte)1 == status )			strDoubleSP = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strDoubleSP = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strDoubleSP_check = "";
}

String strNoSellPoint_check = "checked=\"checked\"";
String strNoSellPoint = LocaleUtil.getMessage(request,"manage_index_unknow");
{
	byte status = DeliveryDB.GMGetNoSellPoint();
	if( (byte)1 == status )			strNoSellPoint = LocaleUtil.getMessage(request,"manage_index_yes");
	else if( (byte)0 == status )	strNoSellPoint = LocaleUtil.getMessage(request,"manage_index_no");
	if( (byte)1 == status )			strNoSellPoint_check = "";
}
%>
<html>
<head>
<link href="../include/style.css" rel="stylesheet" type="text/css">
<title>Управление сервером</title>
<script language=javascript>
function f_doubleexp_submit()
{                                                                                             
	        document.form_doubleexp.doubleexp.value = document.form_doubleexp.expselect.options[document.form_doubleexp.expselect.selectedIndex].value;
		if (document.form_doubleexp.doubleexp.value == "") { 
			alert("Please select experience rate!");
			return false;
		}       
		document.form_doubleexp.submit();
}       
function onsetmaxonlinenum()
{
	if( document.formsetmaxonlinenum.fakemaxnum > document.formsetmaxonlinenum.maxnum.value )
	{
		alert(LocaleUtil.getMessage(request,"manage_index_alert1"));
	}
	return true;
}
</script>
</head>
<body style="background-color:#ffffff;">
<%@include file="/include/header.jsp"%>
<div align="center"><font size="6">Управление сервером</font>
<table width="100%" height="350"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"><tr><td>

&nbsp;<br>
<table border cellpadding=2 align="center" width=600>
<tr><th align=left width=40%>Операция</th><th align=left>Описание</th></tr>

<tr><td>
<form name="formsetmaxonlinenum" action="setmaxonlinenum.jsp" method="post">
&nbsp;Установить предел онлайна&nbsp;&nbsp;<input type="text" name="maxnum" value="<%=strNewMax%>" size="7" maxlength="6">&nbsp;<br>&nbsp;Установить виртуальный предел&nbsp;&nbsp;<input type="text" name="fakemaxnum" value="<%=strNewFakeMax%>" size="7" maxlength="6"><input type="submit" value="ПОДТВЕРЖДАЮ"></form>
</td><td>Установить максимальное количество аккаунтов, которые могут одновременно играть на сервере<br>Тек. онлайн: <font style="color:green;">&nbsp;<%=strCur%></font><br> Возможный онлайн:&nbsp;<font style="color:red;"><%=strMax%>/<%=strFakeMax%></font>&nbsp;</td></tr>


<tr><td>
<form action="setdoubleexp.jsp" name="form_doubleexp" method="post">
<input type="hidden" name="doubleexp" value=""/>
<label>Установить множитель опыта:
	<select name="expselect">
		<option value="1">1</option>
		<option value="1.5">1.5</option>
		<option value="2">2</option>
		<option value="2.5">2.5</option>
		<option value="3">3</option>
		<option value="3">3.5</option>
		<option value="4">4</option>
		<option value="4.5">4.5</option>
		<option value="5">5</option>
		<option value="5.5">5.5</option>
		<option value="6">6</option>
		<option value="6.5">6.5</option>
		<option value="7">7</option>
		<option value="7.5">7.5</option>
		<option value="8">8</option>
		<option value="8.5">8.5</option>
		<option value="9">9</option>
		<option value="9.5">9.5</option>
		<option value="10">10</option>
	</select>
</label>
<input type="button" name="doubleexp_button" value="ПОДТВЕРЖДАЮ" onclick="javascript:f_doubleexp_submit()">
</form>
</td><td>Устанавливает праздничный множитель опыта на указанное значение до следующей перезагрузки сервера<br>Текущее значение:&nbsp;<font style="color:green;"><%=strDoubleExp%></font></td></tr>

<tr><td>
<form action="setnotrade.jsp" method="post">
<input type="checkbox" name="notrade" value="true" <%=strNoTrade_check%> >&nbsp;Запрет на торговлю между игроками&nbsp;&nbsp;<input type="submit" value="Сменить"></form>
</td><td>&nbsp;Текущий статус:&nbsp;<font style="color:green;"><%=strNoTrade%></font>&nbsp;</td></tr>

<tr><td>
<form action="setnoauction.jsp" method="post">
<input type="checkbox" name="noauction" value="true" <%=strNoAuction_check%> >&nbsp;Запрет на все операции аукциона&nbsp;&nbsp;<input type="submit" value="Сменить"></form>
</td><td>&nbsp;Текущий статус:&nbsp;<font style="color:green;"><%=strNoAuction%></font>&nbsp;</td></tr>

<tr><td>
<form action="setnomail.jsp" method="post">
<input type="checkbox" name="nomail" value="true" <%=strNoMail_check%> >&nbsp;Запрет на все почтовые операции&nbsp;&nbsp;<input type="submit" value="Сменить"></form>
</td><td>&nbsp;Текущий статус:&nbsp;<font style="color:green;"><%=strNoMail%></font>&nbsp;</td></tr>

<tr><td>
<form action="setnofaction.jsp" method="post">
<input type="checkbox" name="nofaction" value="true" <%=strNoFaction_check%> >&nbsp;Запрет на все операции с кланами&nbsp;&nbsp;<input type="submit" value="Сменить"></form>
</td><td>&nbsp;Текущий статус:&nbsp;<font style="color:green;"><%=strNoFaction%></font>&nbsp;</td></tr>

<tr><td>
<form action="setdoublemoney.jsp" method="post">
<input type="checkbox" name="doublemoney" value="true" <%=strDoubleMoney_check%> >&nbsp;Удвоенный дроп монет&nbsp;&nbsp;<input type="submit" value="Сменить"></form>
</td><td>&nbsp;Текущий статус:&nbsp;<font style="color:green;"><%=strDoubleMoney%></font>&nbsp;</td></tr>

<tr><td>
<form action="setdoubleobject.jsp" method="post">
<input type="checkbox" name="doubleobject" value="true" <%=strDoubleObject_check%> >&nbsp;Удвоенный дроп предметов&nbsp;&nbsp;<input type="submit" value="Сменить"></form>
</td><td>&nbsp;Текущий статус:&nbsp;<font style="color:green;"><%=strDoubleObject%></font>&nbsp;</td></tr>

<tr><td>
<form action="setdoublesp.jsp" method="post">
<input type="checkbox" name="doublesp" value="true" <%=strDoubleSP_check%> >&nbsp;Удвоенный дух&nbsp;&nbsp;<input type="submit" value="Сменить"></form>
</td><td>&nbsp;Текущий статус:&nbsp;<font style="color:green;"><%=strDoubleSP%></font>&nbsp;</td></tr>

<tr><td>
<form action="shutdowngamefriendly.jsp" method="post">
<font style="color:red;">&nbsp;Выключение сервера.</font><br>&nbsp;Отсчет до выключения:&nbsp;<input type="text" name="waitsecs" value="300" size="7" maxlength="4"><input type="submit" value="ПОДТВЕРЖДАЮ"></form>
</td><td>Выключить сервер с сохранением всех данных и предупреждением игроков.&nbsp;</td></tr>

<tr><td>
<form action="restartgamefriendly.jsp" method="post">
<font style="color:orange;">&nbsp;Перезагрузка сервера.</font><br>&nbsp;Отсчет до перезагрузки:&nbsp;<input type="text" name="waitsecs" value="300" size="7" maxlength="4"><input type="submit" value="ПОДТВЕРЖДАЮ"></form>
</td><td>Перезагрузить сервер с сохранением всех данных и предупреждением игроков. Операция не будет выполнена, если сервер уже выключен.&nbsp;</td></tr>

<tr><td>
<form action="broadcast.jsp" method="post">
<font style="color:red;">&nbsp;Отправка системного сообщения:</font><br><input type="text" name="msg" value="" size="45" maxlength="200"><br><input type="submit" value="Отправить"></form>
</td><td>Отправляет системное сообщение в игровой чат.&nbsp;</td></tr>

<tr><td>
<form action="worldtag.jsp" method="post">
ID локации:&nbsp;<input type="text" name="worldtag" value="" size="7" maxlength="10"><br>
Строка команды:&nbsp;<br><input type="text" name="command" value="" size="45" maxlength="200"><br><input type="submit" value="Отправить"></form>
</td><td>Выполняет команду в определенной локации&nbsp;</td></tr>


</table>
&nbsp;<br>
&nbsp;<br>

</td></tr></table>
</div>
<%@include file="../include/foot.jsp"%>
</body>
</html>

