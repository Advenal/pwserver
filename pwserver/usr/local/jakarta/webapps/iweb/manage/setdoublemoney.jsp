<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.lang.*"%>
<%@page import="protocol.*"%>
<%@page import="com.goldhuman.auth.*"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>

<%
String status = request.getParameter("doublemoney");
if( null == status || !status.equals("true") )
	status = "false";

boolean success = DeliveryDB.GMSetDoubleMoney( status.equals("true") );
LogFactory.getLog("setdoublemoney.jsp").info("setdoublemoney, status=" + status + ",result=" + success + ",operator=" + AuthFilter.getRemoteUser(session) );
%>

<html>
<head>
<link href="../include/style.css" rel="stylesheet" type="text/css">
<title>Менеджер двойного выпадения монет</title>
</head>

<body>
<%@include file="../include/header.jsp"%>
<table width="100%" height="350" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"><tr><td>

<table width="30%" height="200" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td height="350">
<%
	if( success )
		out.print( "Множитель денег установлен &nbsp;&nbsp;&nbsp;&nbsp;" );
	else
		out.print( "Не удалось установить &nbsp;&nbsp;&nbsp;&nbsp;" );
%>
    </td>
  </tr>
</table>

</td></tr></table>
<%@include file="../include/foot.jsp"%>
</body>
</html>

