<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>
<%@page import="java.lang.*"%>
<%@page import="java.util.Calendar.*"%>
<%@page import="java.util.*"%>
<%@page import="com.goldhuman.util.*" %>
<%@page import="protocol.*"%>
<%@page import="com.goldhuman.auth.*"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>
<%@include file="/include/.config.jsp"%>

<html>
<title>Локации и службы</title>
<head>
<%@include file="../include/header.jsp"%>
</head>
<body style="background-color:#ffffff;">
<link href="../include/style.css" rel="stylesheet" type="text/css">
<div align="center"><font size="6">Локации и службы</font>
<table width="800" border="0" cellspacing="0">

<%
	String message = "<br>";

	if(request.getParameter("process") != null)
	{
		Process p;
		String command;
		File working_directory;

		if(request.getParameter("process").compareTo("stopserver") == 0)
		{
			try
			{
				command = pw_server_path + "gamedbd/./gamedbd " + pw_server_path + "gamedbd/gamesys.conf exportclsconfig";
				working_directory = new File(pw_server_path + "gamedbd/");
				p = Runtime.getRuntime().exec(command, null, working_directory);
				p.waitFor();
				Thread.sleep(1000);

				Runtime.getRuntime().exec("pkill -9 gs");
				Runtime.getRuntime().exec("pkill -9 glinkd");
				Runtime.getRuntime().exec("pkill -9 gdeliveryd");
				Runtime.getRuntime().exec("pkill -9 gfactiond");
				Runtime.getRuntime().exec("pkill -9 gacd");
				Runtime.getRuntime().exec("pkill -9 gamedbd");
				Runtime.getRuntime().exec("pkill -9 uniquenamed");
				p = Runtime.getRuntime().exec("ps -A w");
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line;
				while((line = input.readLine()) != null)
				{
					if(line.indexOf("authd") != -1)
					{
						Runtime.getRuntime().exec("kill " + line.substring(0, 5).replace(" ", ""));
					}
					else
						;
				}
				Runtime.getRuntime().exec("pkill -9 logservice");
				Runtime.getRuntime().exec("sync");
				FileWriter fw = new FileWriter(new File("/proc/sys/vm/drop_caches"));
				fw.write("3");
				fw.close();

				Thread.sleep(6000);

				message = "<font color=\"#00cc00\"><b>Сервер выключен</font>";
			}
			catch(Exception e)
			{
				message = "<font color=\"#ee0000\"><b>Выключение не удалось</b></font>";
			}
		}
		else
			;

		if(request.getParameter("process").compareTo("startserver") == 0)
		{
			File f = new File(pw_server_path + "iweb_starter.sh");
			try
			{
				FileWriter fw = new FileWriter(f);
				fw.write("if [ ! -d " + pw_server_path + "logs ]; then\n");
				fw.write("mkdir " + pw_server_path + "logs\n");
				fw.write("fi\n");
				fw.write("cd " + pw_server_path + "logservice; ./logservice logservice.conf >" + pw_server_path + "logs/logservice.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "uniquenamed; ./uniquenamed gamesys.conf >" + pw_server_path + "logs/uniquenamed.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "auth/build; ./authd >" + pw_server_path + "logs/auth.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gamedbd; ./gamedbd gamesys.conf >" + pw_server_path + "logs/gamedbd.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gacd; ./gacd gamesys.conf >" + pw_server_path + "logs/gacd.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gfactiond; ./gfactiond gamesys.conf >" + pw_server_path + "logs/gfactiond.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gdeliveryd; ./gdeliveryd gamesys.conf >" + pw_server_path + "logs/gdeliveryd.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 1 >" + pw_server_path + "logs/glink1.log &\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 2 >" + pw_server_path + "logs/glink2.log &\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 3 >" + pw_server_path + "logs/glink3.log &\n");
				fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 4 >" + pw_server_path + "logs/glink4.log &\n");
				fw.write("sleep 1\n");
				fw.write("cd " + pw_server_path + "gamed; ./gs gs01 >" + pw_server_path + "logs/gs01.log &\n");
				fw.write("sleep 1");
				fw.close();

				command = "sh " + pw_server_path + "iweb_starter.sh";
				working_directory = new File(pw_server_path);
				Runtime.getRuntime().exec("chmod 777 " + pw_server_path + "iweb_starter.sh");
				Runtime.getRuntime().exec(command, null, working_directory);
				Thread.sleep(12000);
				f.delete();

				message = "<font color=\"#00cc00\"><b>Запуск...</font>";
			}
			catch(Exception e)
			{
				f.delete();
				message = "<font color=\"#ee0000\"><b>Запуск не удался</b></font>" + e.getMessage();
			}
		}
		else
		;

		if(request.getParameter("process").compareTo("stopallmaps") == 0)
		{
			try
			{
				int time = Integer.parseInt(request.getParameter("time"));
				if(protocol.DeliveryDB.GMRestartServer(-1, time))
				{
					message = "<font color=\"#00cc00\"><b>Все будет остановлено через " + time + " сек</font>";
				}
				else
				{
					message = "<font color=\"#ee0000\"><b>Остановить не удалось</b></font>";
				}
			}
			catch(Exception e)
			{
				message = "<font color=\"#ee0000\"><b>Остановить не удалось</b></font>";
			}
		}
		else
		;

		if(request.getParameter("process").compareTo("stopmap") == 0)
		{
			try
			{
				String[] maps = request.getParameterValues("map");
				for(int i=0; i<maps.length; i++)
				{
					Runtime.getRuntime().exec("kill " +  maps[i]);
					Thread.sleep(1000);
				}
				message = "<font color=\"#00cc00\"><b>Выключено</b></font>";
			}
			catch(Exception e)
			{
				message = "<font color=\"#ee0000\"><b>Остановить не удалась</b></font>";
			}
		}
		else
			;

		if(request.getParameter("process").compareTo("startmap") == 0)
		{
			File f = new File(pw_server_path + "iweb_map.sh");
			try
			{
				String[] maps = request.getParameterValues("map");
				FileWriter fw = new FileWriter(f);
				for(int i=0; i<maps.length; i++)
				{
					fw.write("cd " + pw_server_path + "gamed; ./gs " + maps[i] + " >" + pw_server_path + "logs/" + maps[i] + ".log &\n");
					fw.write("sleep 1");
				}
				fw.close();

				command = "sh " + pw_server_path + "./iweb_map.sh";
				working_directory = new File(pw_server_path);
				Runtime.getRuntime().exec("chmod 755 " + pw_server_path + "iweb_map.sh");
				Runtime.getRuntime().exec(command, null, working_directory);
				Thread.sleep(1000*maps.length);
				f.delete();
				message = "<font color=\"#00cc00\"><b>Запущено</b></font>";
			}
			catch(Exception e)
			{
				f.delete();
				message = "<font color=\"#ee0000\"><b>Не удалось запустить</b></font>";
			}
		}
		else
			;
		if(request.getParameter("process").compareTo("backup") == 0)
		{
			// Check if another Backup is running

			String line;
			boolean backup_allowed = true;
			p = Runtime.getRuntime().exec("ps -A w");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while((line = input.readLine()) != null)
			{
				if(line.substring(27).indexOf("/./pw_backup.sh") != -1)
				{
					backup_allowed = false;
				}
				else
					;
			}
			input.close();

			if(backup_allowed)
			{
				File f = new File(pw_server_path + "pw_backup.sh");
				try
				{
					String time = (new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss")).format(new java.util.Date());
					FileWriter fw = new FileWriter(f);
					fw.write("cd " + pw_server_path + "\n");
					fw.write("mysqldump -u" + db_user + " -p" + db_password + " " + db_database + " --routines > " + pw_server_path + "pw_backup_" + time + ".sql\n");
					fw.write("sleep 1\n");
					fw.write("tar -zcf " + pw_server_path + "pw_backup_" + time + ".tar.gz " + pw_server_path + " --exclude=pw_backup*\n");
					fw.write("sleep 1\n");
					fw.write("sync; echo 3 > /proc/sys/vm/drop_caches");
					fw.close();

					command = "sh " + pw_server_path + "./pw_backup.sh";
					working_directory = new File(pw_server_path);
					Runtime.getRuntime().exec("chmod 755 " + pw_server_path + "pw_backup.sh");
					Runtime.getRuntime().exec(command, null, working_directory);

					Thread.sleep(3000);

					f.delete();
					message = "<font color=\"#00cc00\"><b>Backup Started -> " + pw_server_path + "pw_backup_" + time + "</b></font>";
				}
				catch(Exception e)
				{
					f.delete();
					message = "<font color=\"#ee0000\"><b>Backup Failed</b></font>";
				}
			}
			else
				;
		}
		else
			;
	}
	else
		;

	// Load

	// maps[*][0] -> process ID / pid
	// maps[*][1] -> map id
	// maps[*][2] -> map name
	String[][] maps;
	if(item_labels.compareTo("pwi") == 0)
	{
		String[][] m = 
		{
		{"0", "gs01", "Мир"}, 
		{"0", "is01", "Город Темных Зверей"}, 
		{"0", "is02", "Могильник Героя"}, 
		{"0", "is05", "Пещера Огненных Скал"}, 
		{"0", "is06", "Логово Бешеных Волков"}, 
		{"0", "is07", "Пещера Ползучих Гадов"}, 
		{"0", "is08", "Зеленый Курган"}, 
		{"0", "is09", "Темная Обитель"}, 
		{"0", "is10", "Инеевый Предел"}, 
		{"0", "is11", "Долина Расхитителей Небес"}, 
		{"0", "is12", "Руины В Джунглях"}, 
		{"0", "is13", "Предел Дьявольских Наслаждений"}, 
		{"0", "is14", "Врата Беспокойных Духов"}, 
		{"0", "is15", "Пещера Драгоценностей"}, 
		{"0", "is16", "Небо Иллюзий"}, 
		{"0", "is17", "Небо Наваждений"}, 
		{"0", "is18", "Дворец Царя Драконов"}, 
		{"0", "is19", "Остров Утренних Слез"}, 
		{"0", "is20", "Змеиный Остров"}, 
		{"0", "is21", "Земли Небожителей"}, 
		{"0", "is22", "Земли Магов И Демонов"}, 
		{"0", "is23", "Земной Ад"}, 
		{"0", "is24", "Чистилище"}, 
		{"0", "is25", "Город Боевых Песен"}, 
		{"0", "is26", "Нирвана"}, 
		{"0", "is27", "Долина Таинственной Луны"}, 
		{"0", "is28", "Ущелье Шень-у"}, 
		{"0", "is29", "Город Инея"}, 
		{"0", "is31", "Сумеречный Храм"}, 
		{"0", "is32", "Куб Рока"}, 
		{"0", "arena01", "Арена ГМ Асура"}, 
		{"0", "arena02", "Арена ГО"}, 
		{"0", "arena03", "Арена ГП"}, 
		{"0", "arena04", "Арена ГД Эвент"}, 
		{"0", "bg01", "ТВ T-3 PvP - 1"}, 
		{"0", "bg02", "ТВ T-3 PvE - 2"}, 
		{"0", "bg03", "ТВ T-2 PvP - 3"}, 
		{"0", "bg04", "ТВ T-2 PvE - 4"}, 
		{"0", "bg05", "ТВ T-1 PvP - 5"}, 
		{"0", "bg06", "ТВ T-1 PvE - 6"}
		};
		maps = m;
	}
	else
	{
		String[][] m = 
		{
		{"0", "gs01", "Мир"}, 
		{"0", "is01", "Город Темных Зверей"}, 
		{"0", "is02", "Могильник Героя"}, 
		{"0", "is05", "Пещера Огненных Скал"}, 
		{"0", "is06", "Логово Бешеных Волков"}, 
		{"0", "is07", "Пещера Скорпионов"}, 
		{"0", "is08", "Зеленый Курган"}, 
		{"0", "is09", "Темная Обитель"}, 
		{"0", "is10", "Инеевый Предел"}, 
		{"0", "is11", "Долина Расхитителей Небес"}, 
		{"0", "is12", "Руины В Джунглях"}, 
		{"0", "is13", "Предел Дьявольских Наслаждений"}, 
		{"0", "is14", "Врата Беспокойных Духов"}, 
		{"0", "is15", "Пещера Драгоценностей"}, 
		{"0", "is16", "Небо Иллюзий"}, 
		{"0", "is17", "Небо Наваждений"}, 
		{"0", "is18", "Дворец Царя Драконов"}, 
		{"0", "is19", "Остров Утренних Слез"}, 
		{"0", "is20", "Змеиный Остров"}, 
		{"0", "is21", "Земли Небожителей"}, 
		{"0", "is22", "Земли Магов И Демонов"}, 
		{"0", "is23", "Земной Ад"}, 
		{"0", "is24", "Чистилище"}, 
		{"0", "is25", "Город Боевых Песен"}, 
		{"0", "is26", "Нирвана"}, 
		{"0", "is27", "Долина Таинственной Луны"}, 
		{"0", "is28", "Ущелье Шень-у"}, 
		{"0", "is29", "Город Инея"}, 
		{"0", "is31", "Сумеречный Храм"}, 
		{"0", "is32", "Куб Рока"}, 
		{"0", "arena01", "Арена ГМ Асура"}, 
		{"0", "arena02", "Арена ГО"}, 
		{"0", "arena03", "Арена ГП"}, 
		{"0", "arena04", "Арена ГД Эвент"}, 
		{"0", "bg01", "ТВ T-3 PvP - 1"}, 
		{"0", "bg02", "ТВ T-3 PvE - 2"}, 
		{"0", "bg03", "ТВ T-2 PvP - 3"}, 
		{"0", "bg04", "ТВ T-2 PvE - 4"}, 
		{"0", "bg05", "ТВ T-1 PvP - 5"}, 
		{"0", "bg06", "ТВ T-1 PvE - 6"}
		};
		maps = m;
	}

	boolean server_running = false;
	boolean backup_running = false;
	int log_count = 0;
	int auth_count = 0;
	int unique_count = 0;
	int gac_count = 0;
	int gfaction_count = 0;
	int gdelivery_count = 0;
	int glink_count = 0;
	int gamedb_count = 0;
	int map_count = 0;
	String[] mem;
	String[] swp;
	String line;
	String process;
	Process p;
	BufferedReader input;

	p = Runtime.getRuntime().exec("free -m");
	input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	input.readLine();
	mem = input.readLine().split("\\s+");
	input.readLine();
	swp = input.readLine().split("\\s+");
	input.close();

	p = Runtime.getRuntime().exec("ps -A w");
	input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	while((line = input.readLine()) != null)
	{
		process = line.substring(27);

		if(process.indexOf("./pw_backup.sh") != -1)
		{
			message += "<br><font color=\"#ee0000\"><b>Выполняется копирование данных<br>Клик <a href=\"index.jsp?page=serverctrl\"><font color=\"#0000cc\">here</font></a> Обновляйте страницу, пока это сообщение не исчезнет ...</b></font>";
		}
		else
			;

		if(process.indexOf("./logservice") != -1)
		{
			log_count++;
			server_running = true;
		}
		else
			;
			
		if(process.indexOf("./authd") != -1)
		{
			auth_count++;
			server_running = true;
		}
		else
			;
			
		if(process.indexOf("./uniquenamed") != -1)
		{
			unique_count++;
			server_running = true;
		}
		else
			;
			
		if(process.indexOf("./gacd") != -1)
		{
			gac_count++;
			server_running = true;
		}
		else
			;
			
		if(process.indexOf("./gfactiond") != -1)
		{
			gfaction_count++;
			server_running = true;
		}
		else
			;
			
		if(process.indexOf("./gdeliveryd") != -1)
		{
			gdelivery_count++;
			server_running = true;
		}
		else
			;
			
		if(process.indexOf("./glinkd") != -1)
		{
			glink_count++;
		}
		else
			;
			
		if(process.indexOf("./gamedb") != -1)
		{
			gamedb_count++;
			server_running = true;
		}
		else
			;
			

		// Check running maps
		for(int i=0; i<maps.length; i++)
		{
			if (process.indexOf("./gs " + maps[i][1]) != -1)
			{
				// set the process id
				maps[i][0] = line.substring(0, 5).replace(" ", "");
				map_count++;
				server_running = true;
			}
			else
				;
		}
	}
	input.close();
%>

<table width="800" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">

<tr bgcolor="#ffffff">
	<td align="center" colspan="2">
		<%= message %>
	</td>
</tr>

<tr bgcolor="#ffffff">
	<td colspan="2">
		<br>
	</td>
</tr>

<tr bgcolor="#ffffff">
<td align="center" valign="top">

	<table width="380" cellpadding="2" cellspacing="0" style="border: 1px solid #ffffff;">
		<tr bgcolor="#d5d5d5">
			<th colspan="4" style="padding: 5;">
				<b>Службы</b>
			</th>
		</tr>
		<tr bgcolor="#d5d5d5">
			<td>
				<b>Память (Мб)</b>
			</td>
			<td align="center">
				<b>Всего<b>
			</td>
			<td align="center">
				<b>#<b>
			</td>
			<td align="center">
				<b>Свободно<b>
			</td>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				RAM:
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
			<%
				out.println(mem[1]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
			<%
				out.println(mem[2]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
			<%
				out.println(mem[3]);
			%>
			</td>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Swap:
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
			<%
				out.println(swp[1]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
			<%
				out.println(swp[2]);
			%>
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
			<%
				out.println(swp[3]);
			%>
			</td>
		</tr>
		<tr bgcolor="#d5d5d5">
			<td style="border-top: 1px solid #ffffff;">
				<b>Описание</b>
			</td>
			<td style="border-top: 1px solid #ffffff;">
				<b>Процесс</b>
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
				<b>#</b>
			</td>
			<td align="center" style="border-top: 1px solid #ffffff;">
				<b>Статус</b>
			</td>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Logservice
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./logservice
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + log_count + "</td>");
				if(log_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Auth Daemon
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./authd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + auth_count + "</td>");
				if(auth_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Unique Name Daemon
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./uniquenamed
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + unique_count + "</td>");
				if(unique_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Game Anti-Cheat Daemon
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./gacd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + gac_count + "</td>");
				if(gac_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Faction Daemon
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./gfactiond
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + gfaction_count + "</td>");
				if(gfaction_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Game Delivery Daemon
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./gdeliveryd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + gdelivery_count + "</td>");
				if(gdelivery_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Game Link Daemon
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./glinkd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + glink_count + "</td>");
				if(glink_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Game Database Daemon
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./gamedbd
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + gamedb_count + "</td>");
				if(gamedb_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td style="border-top: 1px solid #ffffff;">
				Map Service
			</td>
			<td style="border-top: 1px solid #ffffff;">
				./gs
			</td>
			<%
				out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\">" + map_count + "</td>");
				if(map_count > 0)
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#00cc00\">Online</font></td>");
				}
				else
				{
					out.println("<td align=\"center\" style=\"border-top: 1px solid #ffffff;\"><font color=\"#ee0000\">Offline</font></td>");
				}
			%>
		</tr>
		<tr bgcolor="#ffffff">
			<td colspan=4" align="center" style="border-top: 1px solid #ffffff;">
			<form action="instance.jsp?page=instance" method="post" style="margin: 0px;">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr bgcolor="#ffffff">
			<%
					if(server_running)
					{
						out.print("<td style=\"padding-right: 2px;\"><input type=\"hidden\" name=\"process\" value=\"stopserver\"></input>");
						out.print("<input type=\"image\" src=\"../include/btn_stop.jpg\"  style=\"border: 0px;\"></input>");
					}
					else
					{
						out.print("<td><input type=\"hidden\" name=\"process\" value=\"startserver\"></input>");
						out.print("<input type=\"image\" src=\"../include/btn_start.jpg\"  style=\"border: 0px;\"></input></td>");
					}
					out.print("<td style=\"padding-left: 2px;\"><a href=\"instance.jsp?page=instance&process=backup\"><img src=\"../include/btn_backup.jpg\" border=\"0\"></img></a></td>");
			%>
			</tr>
			</table>
			</form>
			</td>
		</tr>
	</table>
</td>

<td align="center" valign="top">

	<table width="380" cellpadding="2" cellspacing="0"  style="border: 1px solid #ffffff;">
		<tr bgcolor="#d5d5d5">
			<th colspan="2" style="padding: 5;">
				<b>Локации</b>
			</th>
		</tr>
		<tr bgcolor="#d5d5d5">
			<td align="center" colspan="2" style="border-top: 1px solid #ffffff;">
				<b>Остановить все<b>
			</td>
		</tr>
		<tr bgcolor="#ffffff">
			<td align="left">
				Задержка (в секундах):
			</td>
			<td align="right">
				<form action="instance.jsp?page=instance&process=stopallmaps" method="post" style="margin: 0px;">
				<table cellpadding="0" cellspacing="0" border="0">
				<tr bgcolor="#ffffff">
				<td><input type="text" name="time" value="300" style="width: 50px; text-align: center;"></input></td>
				<td style="padding-left: 2px;"><input type="image" src="../include/btn_stop.jpg"  style="border: 0px;"></input></td>
				</tr>
				</table>
				</form>
			</td>
		</tr>
		<tr bgcolor="#d5d5d5">
			<td width="50%" align="center">
				<b>Запущено</b>
			</td>
			<td width="50%" align="center">
				<b>Все карты</b>
			</td>
		</tr>
		<tr bgcolor="#ffffff">
			<td align="center">
				<form action="instance.jsp?page=instance&process=stopmap" method="post" solstyle="margin: 0px;">
				<select name="map" size="11" multiple="multiple" style="width: 100%;">
				<%
					for(int i=0; i<maps.length; i++)
					{
						if(maps[i][0].compareTo("0") != 0)
						{
							out.println("<option value=\"" + maps[i][0] + "\">" + maps[i][1] + " : " + maps[i][2] + "</option>");
						}
					}
				%>
				</select>
				<%
						out.print("<input type=\"image\" title=\"Stop the selected Maps\" src=\"../include/btn_stop.jpg\" style=\"border: 0px; padding: 2px;\"></input>");
				%>
				</form>
			</td>
			<td align="center">
				<form action="instance.jsp?page=instance&process=startmap" method="post" style="margin: 0px;">
				<select name="map" size="11" multiple="multiple" style="width: 100%;">
				<%
					for(int i=0; i<maps.length; i++)
					{
						if(maps[i][0].compareTo("0") == 0)
						{
							out.println("<option value=\"" + maps[i][1] + "\">" + maps[i][1] + " : " + maps[i][2] + "</option>");
						}
					}
				%>
				</select>
				<%
						out.print("<input type=\"image\" title=\"Start the selected Maps\" src=\"../include/btn_start.jpg\" style=\"border: 0px; padding: 2px;\"></input>");
				%>
				</form>
			</td>
		</tr>
	</table>
</td>
</tr>
</table>
</div>
<br>
<%@include file="../include/foot.jsp"%>
</body>
</html>