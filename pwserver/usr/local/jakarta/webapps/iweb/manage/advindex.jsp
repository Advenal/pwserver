<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.*"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>
<html>
<head>
<link href="../include/style.css" rel="stylesheet" type="text/css">
<title>Быстрая перезагрузка</title>
</head>
<body>
<%@include file="../include/header.jsp"%>
<table width="100%" height="350"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"><tr><td>

<table border cellpadding=2 align="center" width=600>
<tr><th align=left width=40%>Задача</th><th align=left>Описание</th></tr>
<tr><td><a href="../cgi-bin/control.cgi?action=restartfull">Перезагрузить <font size=3>все службы</font> !</a></td><td>&nbsp;Будут перезагружены (<font color=red>tomcat</font>, <font color=red>authd</font>, <font color=red>gamedbd</font>, <font color=red>uniquenamed</font>, <font color=red>gfactiond</font>, <font color=red>gdeliveryd</font>, <font color=red>logservice</font>, <font color=red>gacd</font>, <font color=red>glinkd</font>, <font color=red>gs</font>)</td></tr>
<tr><td colspan=2>&nbsp;</td></tr>
</table>

<%@include file="../include/foot.jsp"%>
</body>
</html>

