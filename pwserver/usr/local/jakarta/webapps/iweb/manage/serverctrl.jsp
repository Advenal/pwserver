<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.*"%>
<%@page import="com.goldhuman.util.LocaleUtil" %>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>
<%@page import="java.lang.*"%>
<%@page import="java.util.Calendar.*"%>
<%@include file="pwadminconf.jsp"%>
<%
    String message = "<br>";

    // Apply changes
    if (request.getParameter("process") != null) {
        Process p;
        String command;
        File working_directory;

        if (request.getParameter("process").compareTo("stopserver") == 0) {
            try {
                command = pw_server_path + "gamedbd/./gamedbd " + pw_server_path + "gamedbd/gamesys.conf exportclsconfig";
                working_directory = new File(pw_server_path + "gamedbd/");
                p = Runtime.getRuntime().exec(command, null, working_directory);
                p.waitFor();
                Thread.sleep(1000);

                Runtime.getRuntime().exec("pkill -9 gs");
                Runtime.getRuntime().exec("pkill -9 glinkd");
                Runtime.getRuntime().exec("pkill -9 gdeliveryd");
                Runtime.getRuntime().exec("pkill -9 gfactiond");
                Runtime.getRuntime().exec("pkill -9 gacd");
                Runtime.getRuntime().exec("pkill -9 gamedbd");
                Runtime.getRuntime().exec("pkill -9 uniquenamed");
                p = Runtime.getRuntime().exec("ps -A w");
                BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                while ((line = input.readLine()) != null) {
                    if (line.indexOf("authd") != -1) {
                        Runtime.getRuntime().exec("kill " + line.substring(0, 5).replace(" ", ""));
                    }
                }
                Runtime.getRuntime().exec("pkill -9 logservice");
                Runtime.getRuntime().exec("sync");
// drop_caches -> permission denied
//				FileWriter fw = new FileWriter(new File("/proc/sys/vm/drop_caches"));
//				fw.write("3");
//				fw.close();

                Thread.sleep(6000);

                message = "<font color=\"#00cc00\"><b>Server Turned Off</font>";
            } catch (Exception e) {
                message = "<font color=\"#ee0000\"><b>Turning Off Server Failed</b></font>";
            }
        }

        if (request.getParameter("process").compareTo("startserver") == 0) {
            File f = new File(pw_server_path + "iweb_starter.sh");
            try {
                FileWriter fw = new FileWriter(f);
                fw.write("if [ ! -d " + pw_logs_path + " ]; then\n");
                fw.write("mkdir " + pw_logs_path + "\n");
                fw.write("fi\n");
                fw.write("cd " + pw_server_path + "logservice; ./logservice logservice.conf >" + pw_logs_path + "Logservice.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "uniquenamed; ./uniquenamed gamesys.conf >" + pw_logs_path + "uniquenamed.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "auth/build/; /bin/sh ./authd.sh start >" + pw_logs_path + "auth.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "gamedbd; ./gamedbd gamesys.conf >" + pw_logs_path + "gamedbd.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "gacd; ./gacd gamesys.conf >" + pw_logs_path + "gacd.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "gfactiond; ./gfactiond gamesys.conf >" + pw_logs_path + "gfactiond.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "gdeliveryd; ./gdeliveryd gamesys.conf >" + pw_logs_path + "gdeliveryd.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 1 >" + pw_logs_path + "glink1.log &\n");
                fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 2 >" + pw_logs_path + "glink2.log &\n");
                fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 3 >" + pw_logs_path + "glink3.log &\n");
                fw.write("cd " + pw_server_path + "glinkd; ./glinkd gamesys.conf 4 >" + pw_logs_path + "glink4.log &\n");
                fw.write("sleep 1\n");
                fw.write("cd " + pw_server_path + "gamed; ./gs gs01 >" + pw_logs_path + "gs01.log &\n");
                fw.write("sleep 1\n");
                fw.write("rm foo\n");
                fw.write("sleep 1");
                fw.close();

                command = "sh " + pw_server_path + "iweb_starter.sh";
                working_directory = new File(pw_server_path);
                Runtime.getRuntime().exec("chmod 777 " + pw_server_path + "iweb_starter.sh");
                Runtime.getRuntime().exec(command, null, working_directory);
                Thread.sleep(12000);
                f.delete();

                message = "<font color=\"#00cc00\"><b>Server is Starting Up... it could take some minutes till server is fully up and running</font>";
            } catch (Exception e) {
                f.delete();
                message = "<font color=\"#ee0000\"><b>Starting Up Server Failed</b></font>" + e.getMessage();
            }
        }

        if (request.getParameter("process").compareTo("stopallmaps") == 0) {
            try {
                int time = Integer.parseInt(request.getParameter("time"));
                if (protocol.DeliveryDB.GMRestartServer(-1, time)) {
                    message = "<font color=\"#00cc00\"><b>All Maps will be stopped in " + time + " seconds</font>";
                } else {
                    message = "<font color=\"#ee0000\"><b>Setting Timer to stop Maps Failed</b></font>";
                }
            } catch (Exception e) {
                message = "<font color=\"#ee0000\"><b>Setting Timer to stop Maps Failed</b></font>";
            }
        }

        if (request.getParameter("process").compareTo("stopmap") == 0) {
            try {
                String[] maps = request.getParameterValues("map");
                for (int i = 0; i < maps.length; i++) {
                    Runtime.getRuntime().exec("kill " + maps[i]);
                    Thread.sleep(1000);
                }
                message = "<font color=\"#00cc00\"><b>Map(s) Stopped</b></font>";
            } catch (Exception e) {
                message = "<font color=\"#ee0000\"><b>Stopping Map(s) Failed</b></font>";
            }
        }

        if (request.getParameter("process").compareTo("startmap") == 0) {
            File f = new File(pw_server_path + "iweb_map.sh");
            try {
                String[] maps = request.getParameterValues("map");
                FileWriter fw = new FileWriter(f);
                for (int i = 0; i < maps.length; i++) {
                    fw.write("cd " + pw_server_path + "gamed; ./gs " + maps[i] + " >" + pw_logs_path + "/gs" + maps[i] + ".log &\n");
                    fw.write("sleep 1\n");
                }
                fw.write("rm foo\n");
                fw.write("sleep 1");
                fw.close();

                command = "sh " + pw_server_path + "./iweb_map.sh";
                working_directory = new File(pw_server_path);
                Runtime.getRuntime().exec("chmod 755 " + pw_server_path + "iweb_map.sh");
                Runtime.getRuntime().exec(command, null, working_directory);
                Thread.sleep(1000 * maps.length + 1);
                f.delete();
                message = "<font color=\"#00cc00\"><b>Map(s) Started</b></font>";
            } catch (Exception e) {
                f.delete();
                message = "<font color=\"#ee0000\"><b>Starting Map(s) Failed</b></font>";
            }
        }
        
        if (request.getParameter("process").compareTo("colsesystem") == 0 ){
        	try{
        		Runtime.getRuntime().exec("shutdown -h now");
        	}catch (Exception e) {
        	}
        }

        if (request.getParameter("process").compareTo("backup") == 0) {
            // Check if another Backup is running

            String line;
            boolean backup_allowed = true;
            p = Runtime.getRuntime().exec("ps -A w");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                if (line.substring(27).indexOf("/./pw_backup.sh") != -1) {
                    backup_allowed = false;
                }
            }
            input.close();

            if (backup_allowed) {
                File f = new File(pw_server_path + "pw_backup.sh");
                try {
                    String time = (new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss")).format(new java.util.Date());
                    FileWriter fw = new FileWriter(f);
                    fw.write("cd " + pw_server_path + "\n");
                    fw.write("mysqldump -u" + db_user + " -p" + db_password + " " + db_database + " --routines > " + pw_server_path + "pw_backup_" + time + ".sql\n");
                    fw.write("sleep 1\n");
                    fw.write("tar -zcf " + pw_server_path + "pw_backup_" + time + ".tar.gz " + pw_server_path + " --exclude=pw_backup*\n");
                    fw.write("sleep 1\n");
// drop_caches -> permission denied
//					fw.write("sync; echo 3 > /proc/sys/vm/drop_caches");
                    fw.write("sync");
                    fw.close();

                    command = "sh " + pw_server_path + "./pw_backup.sh";
                    working_directory = new File(pw_server_path);
                    Runtime.getRuntime().exec("chmod 755 " + pw_server_path + "pw_backup.sh");
                    Runtime.getRuntime().exec(command, null, working_directory);

                    Thread.sleep(3000);

                    f.delete();
                    message = "<font color=\"#00cc00\"><b>Backup Started -> " + pw_server_path + "pw_backup_" + time + "</b></font>";
                } catch (Exception e) {
                    f.delete();
                    message = "<font color=\"#ee0000\"><b>Backup Failed</b></font>";
                }
            }
        }
    }


    // Load

    // maps[*][0] -> process ID / pid
    // maps[*][1] -> map id
    // maps[*][2] -> map name
    String[][] maps;
    if (item_labels.compareTo("pwi") == 0) {
        String[][] m = {
            {"0", "gs01", LocaleUtil.getMessage(request, "gs01")},
            {"0", "is01", LocaleUtil.getMessage(request, "is01")},
            {"0", "is02", LocaleUtil.getMessage(request, "is02")},
            {"0", "is03", LocaleUtil.getMessage(request, "is03")},
            {"0", "is04", LocaleUtil.getMessage(request, "is04")},
            {"0", "is05", LocaleUtil.getMessage(request, "is05")},
            {"0", "is06", LocaleUtil.getMessage(request, "is06")},
            {"0", "is07", LocaleUtil.getMessage(request, "is07")},
            {"0", "is08", LocaleUtil.getMessage(request, "is08")},
            {"0", "is09", LocaleUtil.getMessage(request, "is09")},
            {"0", "is10", LocaleUtil.getMessage(request, "is10")},
            {"0", "is11", LocaleUtil.getMessage(request, "is11")},
            {"0", "is12", LocaleUtil.getMessage(request, "is12")},
            {"0", "is13", LocaleUtil.getMessage(request, "is13")},
            {"0", "is14", LocaleUtil.getMessage(request, "is14")},
            {"0", "is15", LocaleUtil.getMessage(request, "is15")},
            {"0", "is16", LocaleUtil.getMessage(request, "is16")},
            {"0", "is17", LocaleUtil.getMessage(request, "is17")},
            {"0", "is18", LocaleUtil.getMessage(request, "is18")},
            {"0", "is19", LocaleUtil.getMessage(request, "is19")},
            {"0", "is20", LocaleUtil.getMessage(request, "is20")},
            {"0", "is21", LocaleUtil.getMessage(request, "is21")},
            {"0", "is22", LocaleUtil.getMessage(request, "is22")},
            {"0", "is23", LocaleUtil.getMessage(request, "is23")},
            {"0", "is24", LocaleUtil.getMessage(request, "is24")},
            {"0", "is25", LocaleUtil.getMessage(request, "is25")},
            {"0", "is26", LocaleUtil.getMessage(request, "is26")},
            {"0", "is27", LocaleUtil.getMessage(request, "is27")},
            {"0", "is28", LocaleUtil.getMessage(request, "is28")},
            {"0", "is29", LocaleUtil.getMessage(request, "is29")},
            {"0", "is30", LocaleUtil.getMessage(request, "is30")},
            {"0", "is31", LocaleUtil.getMessage(request, "is31")},
            {"0", "is32", LocaleUtil.getMessage(request, "is32")},
            {"0", "is33", LocaleUtil.getMessage(request, "is33")},
            {"0", "is34", LocaleUtil.getMessage(request, "is34")},
            {"0", "is35", LocaleUtil.getMessage(request, "is35")},
	    {"0", "is37", LocaleUtil.getMessage(request, "is37")},
	    {"0", "is38", LocaleUtil.getMessage(request, "is38")},
	    {"0", "is39", LocaleUtil.getMessage(request, "is39")},
	    {"0", "is40", LocaleUtil.getMessage(request, "is40")},
            {"0", "arena01", LocaleUtil.getMessage(request, "arena01")},
            {"0", "arena02", LocaleUtil.getMessage(request, "arena02")},
            {"0", "arena03", LocaleUtil.getMessage(request, "arena03")},
            {"0", "arena04", LocaleUtil.getMessage(request, "arena04")},
            {"0", "arena05", LocaleUtil.getMessage(request, "arena05")},
            {"0", "bg01", LocaleUtil.getMessage(request, "bg01")},
            {"0", "bg02", LocaleUtil.getMessage(request, "bg02")},
            {"0", "bg03", LocaleUtil.getMessage(request, "bg03")},
            {"0", "bg04", LocaleUtil.getMessage(request, "bg04")},
            {"0", "bg05", LocaleUtil.getMessage(request, "bg05")},
            {"0", "bg06", LocaleUtil.getMessage(request, "bg06")}
        };
        maps = m;
    } else {
        String[][] m = {
            {"0", "gs01", LocaleUtil.getMessage(request, "gs01")},
            {"0", "is01", LocaleUtil.getMessage(request, "is01")},
            {"0", "is02", LocaleUtil.getMessage(request, "is02")},
            {"0", "is03", LocaleUtil.getMessage(request, "is03")},
            {"0", "is04", LocaleUtil.getMessage(request, "is04")},
            {"0", "is05", LocaleUtil.getMessage(request, "is05")},
            {"0", "is06", LocaleUtil.getMessage(request, "is06")},
            {"0", "is07", LocaleUtil.getMessage(request, "is07")},
            {"0", "is08", LocaleUtil.getMessage(request, "is08")},
            {"0", "is09", LocaleUtil.getMessage(request, "is09")},
            {"0", "is10", LocaleUtil.getMessage(request, "is10")},
            {"0", "is11", LocaleUtil.getMessage(request, "is11")},
            {"0", "is12", LocaleUtil.getMessage(request, "is12")},
            {"0", "is13", LocaleUtil.getMessage(request, "is13")},
            {"0", "is14", LocaleUtil.getMessage(request, "is14")},
            {"0", "is15", LocaleUtil.getMessage(request, "is15")},
            {"0", "is16", LocaleUtil.getMessage(request, "is16")},
            {"0", "is17", LocaleUtil.getMessage(request, "is17")},
            {"0", "is18", LocaleUtil.getMessage(request, "is18")},
            {"0", "is19", LocaleUtil.getMessage(request, "is19")},
            {"0", "is20", LocaleUtil.getMessage(request, "is20")},
            {"0", "is21", LocaleUtil.getMessage(request, "is21")},
            {"0", "is22", LocaleUtil.getMessage(request, "is22")},
            {"0", "is23", LocaleUtil.getMessage(request, "is23")},
            {"0", "is24", LocaleUtil.getMessage(request, "is24")},
            {"0", "is25", LocaleUtil.getMessage(request, "is25")},
            {"0", "is26", LocaleUtil.getMessage(request, "is26")},
            {"0", "is27", LocaleUtil.getMessage(request, "is27")},
            {"0", "is28", LocaleUtil.getMessage(request, "is28")},
            {"0", "is29", LocaleUtil.getMessage(request, "is29")},
            {"0", "is30", LocaleUtil.getMessage(request, "is30")},
            {"0", "is31", LocaleUtil.getMessage(request, "is31")},
            {"0", "is32", LocaleUtil.getMessage(request, "is32")},
            {"0", "is33", LocaleUtil.getMessage(request, "is33")},
            {"0", "is34", LocaleUtil.getMessage(request, "is34")},
            {"0", "is35", LocaleUtil.getMessage(request, "is35")},
	    {"0", "is37", LocaleUtil.getMessage(request, "is37")},
	    {"0", "is38", LocaleUtil.getMessage(request, "is38")},
	    {"0", "is39", LocaleUtil.getMessage(request, "is39")},
	    {"0", "is40", LocaleUtil.getMessage(request, "is40")},
            {"0", "arena01", LocaleUtil.getMessage(request, "arena01")},
            {"0", "arena02", LocaleUtil.getMessage(request, "arena02")},
            {"0", "arena03", LocaleUtil.getMessage(request, "arena03")},
            {"0", "arena04", LocaleUtil.getMessage(request, "arena04")},
            {"0", "arena05", LocaleUtil.getMessage(request, "arena05")},
            {"0", "bg01", LocaleUtil.getMessage(request, "bg01")},
            {"0", "bg02", LocaleUtil.getMessage(request, "bg02")},
            {"0", "bg03", LocaleUtil.getMessage(request, "bg03")},
            {"0", "bg04", LocaleUtil.getMessage(request, "bg04")},
            {"0", "bg05", LocaleUtil.getMessage(request, "bg05")},
            {"0", "bg06", LocaleUtil.getMessage(request, "bg06")}
        };
        maps = m;
    }

    boolean server_running = false;
    boolean backup_running = false;
    int log_count = 0;
    int auth_count = 0;
    int unique_count = 0;
    int gac_count = 0;
    int gfaction_count = 0;
    int gdelivery_count = 0;
    int glink_count = 0;
    int gamedb_count = 0;
    int map_count = 0;
    String[] mem;
    String[] swp;
    String line;
    String process;
    Process p;
    BufferedReader input;

    p = Runtime.getRuntime().exec("free -m");
    input = new BufferedReader(new InputStreamReader(p.getInputStream()));
    input.readLine();
    mem = input.readLine().split("\\s+");
    input.readLine();
    swp = input.readLine().split("\\s+");
    input.close();

    p = Runtime.getRuntime().exec("ps -A w");
    input = new BufferedReader(new InputStreamReader(p.getInputStream()));
    while ((line = input.readLine()) != null) {
        process = line.substring(27);

        if (process.indexOf("./pw_backup.sh") != -1) {
            message += "<br><font color=\"#ee0000\"><b>A Server Backup is Running please wait until Backup is finished!<br>Click <a href=\"?page=serverctrl\"><font color=\"#0000cc\">here</font></a> from Time to Time until this Message disappears...</b></font>";
        }

        if (process.indexOf("./logservice") != -1) {
            log_count++;
            server_running = true;
        }
        //if(process.indexOf("./authd.sh") != -1)
        if (process.indexOf("/bin/sh ./authd.sh start") != -1) {
            auth_count++;
            server_running = true;
        }
        if (process.indexOf("./uniquenamed") != -1) {
            unique_count++;
            server_running = true;
        }
        if (process.indexOf("./gacd") != -1) {
            gac_count++;
            server_running = true;
        }
        if (process.indexOf("./gfactiond") != -1) {
            gfaction_count++;
            server_running = true;
        }
        if (process.indexOf("./gdeliveryd") != -1) {
            gdelivery_count++;
            server_running = true;
        }
        if (process.indexOf("./glinkd") != -1) {
            glink_count++;
        }
        if (process.indexOf("./gamedb") != -1) {
            gamedb_count++;
            server_running = true;
        }

        // Check running maps
        for (int i = 0; i < maps.length; i++) {
            if (process.indexOf("./gs " + maps[i][1]) != -1) {
                // set the process id
                maps[i][0] = line.substring(0, 5).replace(" ", "");
                map_count++;
                server_running = true;
            }
        }
    }
    input.close();
%>
<html>
<head>
<link href="../include/style.css" rel="stylesheet" type="text/css">
<title><%= LocaleUtil.getMessage(request, "include_header_advmng")%></title>
</head>
<body>
<%@include file="../include/header.jsp"%>
<table width="100%" height="350"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td align="center"><table width="800" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td align="center" colspan="2"><%=message%></td>
        </tr>
        <tr>
          <td colspan="2"><br></td>
        </tr>
        <tr>
          <td align="center" valign="top"><table width="380" cellpadding="2" cellspacing="0" style="border: 1px solid #cccccc;">
              <tr>
                <th colspan="4" style="padding:5px;"> <b>SERVICES</b> </th>
              </tr>
              <tr bgcolor="#f0f0f0">
                <td><b>Memory Usage (MByte)</b></td>
                <td align="center"><b>Total<b></td>
                <td align="center"><b>Used<b></td>
                <td align="center"><b>Free<b></td>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> RAM: </td>
                <td align="center" style="border-top: 1px solid #cccccc;"><% out.println(mem[1]);%></td>
                <td align="center" style="border-top: 1px solid #cccccc;"><% out.println(mem[2]);%></td>
                <td align="center" style="border-top: 1px solid #cccccc;"><% out.println(mem[3]);%></td>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Swap: </td>
                <td align="center" style="border-top: 1px solid #cccccc;"><% out.println(swp[1]);%></td>
                <td align="center" style="border-top: 1px solid #cccccc;"><% out.println(swp[2]);%></td>
                <td align="center" style="border-top: 1px solid #cccccc;"><% out.println(swp[3]);%></td>
              </tr>
              <tr bgcolor="#f0f0f0">
                <td style="border-top: 1px solid #cccccc;"><b>Service Description</b></td>
                <td style="border-top: 1px solid #cccccc;"><b>Process</b></td>
                <td align="center" style="border-top: 1px solid #cccccc;"><b>#</b></td>
                <td align="center" style="border-top: 1px solid #cccccc;"><b>Status</b></td>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Logservice </td>
                <td style="border-top: 1px solid #cccccc;"> ./logservice </td>
                <%
                out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + log_count + "</td>");
                if (log_count > 0) {
                   out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
                } else {
                   out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
                }
                %>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Auth Daemon </td>
                <td style="border-top: 1px solid #cccccc;"> ./authd </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + auth_count + "</td>");
					if (auth_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Unique Name Daemon </td>
                <td style="border-top: 1px solid #cccccc;"> ./uniquenamed </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + unique_count + "</td>");
					if (unique_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Game Anti-Cheat Daemon </td>
                <td style="border-top: 1px solid #cccccc;"> ./gacd </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + gac_count + "</td>");
					if (gac_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Faction Daemon </td>
                <td style="border-top: 1px solid #cccccc;"> ./gfactiond </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + gfaction_count + "</td>");
					if (gfaction_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Game Delivery Daemon </td>
                <td style="border-top: 1px solid #cccccc;"> ./gdeliveryd </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + gdelivery_count + "</td>");
					if (gdelivery_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Game Link Daemon </td>
                <td style="border-top: 1px solid #cccccc;"> ./glinkd </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + glink_count + "</td>");
					if (glink_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Game Database Daemon </td>
                <td style="border-top: 1px solid #cccccc;"> ./gamedbd </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + gamedb_count + "</td>");
					if (gamedb_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td style="border-top: 1px solid #cccccc;"> Map Service </td>
                <td style="border-top: 1px solid #cccccc;"> ./gs </td>
                <%
					out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\">" + map_count + "</td>");
					if (map_count > 0) {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#00cc00\">Online</font></td>");
					} else {
						out.println("<td align=\"center\" style=\"border-top: 1px solid #cccccc;\"><font color=\"#ee0000\">Offline</font></td>");
					}
				%>
              </tr>
              <tr>
                <td colspan="4" align="center" style="border-top: 1px solid #cccccc;">
                	<form action="" method="post" style="margin: 0px;">
                    <table cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <%
							if (server_running) {
								out.print("<td style=\"padding-right: 2px;\"><input type=\"hidden\" name=\"process\" value=\"stopserver\"></input>");
								out.print("<input type=\"submit\" value=\"" + LocaleUtil.getMessage(request, "ServerStop") + "\"></input>");
							} else {
								out.print("<td><input type=\"hidden\" name=\"process\" value=\"startserver\"></input>");
								out.print("<input type=\"submit\" value=\"" + LocaleUtil.getMessage(request, "ServerStart") + "\" ></input></td>");
							}
							out.print("<td style=\"padding-left: 2px;\"><a href=\"?process=backup\">" + LocaleUtil.getMessage(request, "Serverbackup") + "</a></td>");
							out.print("<td style=\"padding-left: 2px;\"><a href=\"javascript:confirm('close?') ? window.location.href='?process=colsesystem' : '';\">poweroff</a></td>");
  
						%>
                      </tr>
                    </table>
                  </form></td>
              </tr>
            </table></td>
          <td align="center" valign="top"><table width="380" cellpadding="2" cellspacing="0"  style="border: 1px solid #cccccc;">
              <tr>
                <th colspan="2" style="padding: 5;"> <b>MAPS</b> </th>
              </tr>
              <tr bgcolor="#f0f0f0">
                <td align="center" colspan="2" style="border-top: 1px solid #cccccc;"><b>Stop all Maps softly<b></td>
              </tr>
              <tr>
                <td align="left"> Delay (seconds): </td>
                <td align="right"><form action="?process=stopallmaps" method="post" style="margin: 0px;">
                    <table cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td><input type="text" name="time" value="300" style="width: 50px; text-align: center;">
                          </input></td>
                        <td style="padding-left: 2px;"><input type="submit" value="<%=LocaleUtil.getMessage(request, "ServerStop")%>" >
                          </input></td>
                      </tr>
                    </table>
                  </form></td>
              </tr>
              <tr bgcolor="#f0f0f0">
                <td width="50%" align="center"><b>Online Maps</b></td>
                <td width="50%" align="center"><b>Available Maps</b></td>
              </tr>
              <tr>
                <td align="center">
                <form action="?process=stopmap" method="post" style="margin: 0px;">
                    <select name="map" size="11" multiple="multiple" style="width: 100%;">
                      <%
						  for (int i = 0; i < maps.length; i++) {
							  if (maps[i][0].compareTo("0") != 0) {
								  out.println("<option value=\"" + maps[i][0] + "\">" + maps[i][1] + " : " + maps[i][2] + "</option>");
							  }
						  }
					  %>
                    </select>
                    <%
					  out.print("<input type=\"submit\" title=\"Stop the selected Maps\" value=\"" + LocaleUtil.getMessage(request, "ServerStop") + "\" />");
				   %>
                </form>
                </td>
                <td align="center"><form action="?process=startmap" method="post" style="margin: 0px;">
                    <select name="map" size="11" multiple="multiple" style="width: 100%;">
                      <%
						  for (int i = 0; i < maps.length; i++) {
							  if (maps[i][0].compareTo("0") == 0) {
								  out.println("<option value=\"" + maps[i][1] + "\">" + maps[i][1] + " : " + maps[i][2] + "</option>");
							  }
						  }
					  %>
                    </select>
                <%
					out.print("<input type=\"submit\" title=\"Start the selected Maps\" value=\"" + LocaleUtil.getMessage(request, "ServerStart") + "\"></input>");
				%>
                  </form></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
<%@include file="../include/foot.jsp"%>
</body>
</html>
